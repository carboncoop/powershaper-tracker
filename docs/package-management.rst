Package management with pip-tools
=================================

This is a short guide and not intended to be exhaustive.

pip-tools is a package that enables Python dependency management using standard Python
tooling (i.e. pip and requirements.txt) rather than a custom file format or package
manager.  We use it across Carbon Co-op projects.  It has two parts: pip-compile (used
for managing requirements.txt files) and pip-sync (used for installing from them).


Architecture
------------

In our requirements folder we have three .in files and two .txt files.  The .in files
are used by pip-compile as input to produce .txt files of the same name.

production.in contains a list of all the packages that are required for a production
deployment.  local.in contains a list of the *additional* packages required for local
use.  pip-compile takes these and produces production.txt and local.txt which can be
used by pip install or pip-sync to actually install packages.

production.in or local.in generally contain a list of *unversioned* packages.
pip-compile's role is to produce a *versioned* list of packages to installed that
do not have any conflicting requirements.

Both the .in and .txt files are kept in version control.


Adding a dependency
-------------------

1. Decide if it's a local dependency or a production one.  If it's for local deveopment
   only, put its pypi package name in requirements/local.in.  Otherwise, put it in
   requirements/production.in.
2. Run `make pip-compile`. This will recompile both package files and give you a diff
   of the changes.  (If you want to know what commands this runs, just look in
   Makefile.)
3. Run `make sync`. This syncs up your local virtual environment with the packages now
   specified in local.txt.


Removing a dependency
---------------------

Take it out of the relevant .in file and run `make pip-compile` and `make pip-sync`.


Upgrading a dependency
----------------------

Run `make pip-upgrade pkg=<package you want to upgrade>` and then run `make pip-sync`.


Upgrading all dependencies
--------------------------

Run `make pip-upgrade-all` and then run `make pip-sync`.
