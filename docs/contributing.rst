Contributing
===============

This project
PowerShaper Tracker is a web-based tool created by Carbon Co-op that allows in-depth visualisation of metered energy savings over a period of time. It is primarily designed for retrofit professionals to calculate the impact of energy efficiency measures across an entire property portfolio. It may also be beneficial as part of a  retrofit evaluation process for individual homes, either by design professionals or experienced  householders.
To get started on this project, follow the instructions linked below: https://gitlab.com/carboncoop/powershaper-tracker/-/blob/main/docs/getting-started.rst.

Types of Contributions
======================
Carbon Co-op very much welcomes contributions from users and external developers, following the guide below. We aim to be an open and welcoming part of the open source community and ask that all issues, discussions and contributions comply with our Code of Conduct. https://gitlab.com/carboncoop/powershaper-tracker/-/blob/main/CODE_OF_CONDUCT.md

Report Bugs
-----------
Report bugs at https://gitlab.com/carboncoop/powershaper-tracker/-/issues.
If you are reporting a bug, please include:

Any details about your local setup that might be helpful in troubleshooting.
Detailed steps to reproduce the bug.


Fix Bugs
--------
Look through the GitLab issues for bugs.

Implement Features
------------------
Look through the GitLab issues for features.

Write Documentation
-------------------
PowerShaper Tracker could always use more documentation, whether as part of the
official PoweShaper Tracker docs, in docstrings, or even on the web in blog posts,
articles, and such.

Submit Feedback
---------------
The best way to send feedback is to file an issue at https://gitlab.com/carboncoop/powershaper-tracker/issues.

If you are proposing a feature:
-Explain in detail how it would work.
-Keep the scope as narrow as possible, to make it easier to implement.

===============

All ‘over the wall’ contributions are required to be signed off by the contributor before submitting a merge request for review.

How to sign-off contributions
Using the CLI client:
git commit -s   or   git commit -signoff

Using the WEB IDE add the following as your final line of your commit:
Signed-off-by: Name <email>

(note any commits that are not signed off will automatically be denied/removed)

In signing you the code you areconfirming that you have the right to share the code you are contributing, in line with the certificate below.

===============

Developer Certificate of Origin
===============================
Version 1.1

Copyright (C) 2004, 2006 The Linux Foundation and its contributors.

Everyone is permitted to copy and distribute verbatim copies of this
license document, but changing it is not allowed.


Developer's Certificate of Origin 1.1

By making a contribution to this project, I certify that:

(a) The contribution was created in whole or in part by me and I
   have the right to submit it under the open source license
   indicated in the file; or

(b) The contribution is based upon previous work that, to the best
   of my knowledge, is covered under an appropriate open source
   license and I have the right under that license to submit that
   work with modifications, whether created in whole or in part
   by me, under the same open source license (unless I am
   permitted to submit under a different license), as indicated
   in the file; or

(c) The contribution was provided directly to me by some other
   person who certified (a), (b) or (c) and I have not modified
   it.

(d) I understand and agree that this project and the contribution
   are public and that a record of the contribution (including all
   personal information I submit with it, including my sign-off) is
   maintained indefinitely and may be redistributed consistent with
   this project or the open source license(s) involved.
