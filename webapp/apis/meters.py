import datetime
from dataclasses import dataclass
from typing import Any
from typing import List
from typing import Literal

import requests
import typedload.dataloader
from dateutil.parser import parse
from typedload.exceptions import TypedloadTypeError

from webapp.base import enums as base_enums


def get_meterconsent_info(token, uuid):
    """Collate some data relating to a given meter consent."""
    results = get_meterconsent_list(token=token, uuid=uuid)

    postcode = results[0].postcode
    last_four_digits = results[0].consent_last_four_digits
    electricity_consents = list(
        filter(lambda meter: meter.type == "electricity", results)
    )
    gas_consents = list(filter(lambda meter: meter.type == "gas", results))

    return (
        postcode,
        last_four_digits,
        electricity_consents[0] if len(electricity_consents) > 0 else None,
        gas_consents[0] if len(gas_consents) > 0 else None,
    )


@dataclass
class MeterConsentLinks:
    records: str


@dataclass
class MeterConsentRange:
    earliest: datetime.datetime
    latest: datetime.datetime


@dataclass
class MeterConsentInfo:
    consent_last_four_digits: str
    consent_name: str
    consent_uuid: str
    links: MeterConsentLinks
    range: MeterConsentRange | None
    status: Literal["active", "revoked"]
    type: Literal["gas", "electricity"]
    postcode: str


def get_meterconsent_list(token, uuid=None) -> List[MeterConsentInfo]:
    if uuid is not None:
        params = {"meter": uuid}
    else:
        params = {}

    try:
        response = requests.get(
            url="https://app.powershaper.io/meters/api/v1/meters/",
            params=params,
            headers={"Authorization": f"Token {token}"},
            timeout=5,
        )
    except requests.RequestException:
        raise LookupError("Network error fetching meter list")

    if response.status_code == 403:
        raise LookupError("Invalid or no authorisation token supplied.")
    if response.status_code != 200:
        raise LookupError("HTTP error fetching meter list")

    try:
        results = response.json()
    except requests.InvalidJSONError:
        raise LookupError("Error decoding server response")

    result = _loader(results, List[MeterConsentInfo])
    return result


@dataclass
class MeterDataRecord:
    carbon_kg: float
    energy_kwh: float
    error_count: int
    time: datetime.datetime


def get_meter_records(
    *,
    token: str,
    meter_uuid: str,
    type: base_enums.MeterType,
    start: datetime.date,
    end: datetime.date,
) -> List[MeterDataRecord]:
    if type == base_enums.MeterType.GAS:
        fetch_type = "gas"
    elif type == base_enums.MeterType.ELEC:
        fetch_type = "electricity"

    try:
        response = requests.get(
            url=f"https://app.powershaper.io/meters/api/v1/meter/{meter_uuid}/{fetch_type}",
            headers={"Authorization": f"Token {token}"},
            params={
                "start": start.isoformat(),
                "end": end.isoformat(),
                "aggregate": "none",
                "tz": "Etc/UTC",
                "pad_ending": True,
            },
            timeout=50,
        )
    except requests.RequestException:
        raise LookupError("Network error fetching meter records")

    if response.status_code != 200:
        raise LookupError("HTTP error fetching meter records")

    try:
        results = response.json()
    except requests.InvalidJSONError:
        raise LookupError("Error decoding server response")

    result = _loader(results, List[MeterDataRecord])
    return result


def _datetimeload(
    _loader: typedload.dataloader.Loader, value: Any, type_
) -> datetime.date | datetime.time | datetime.datetime:
    """Parse a datetime from an ISO date/time string."""
    try:
        return parse(value)
    except Exception as e:
        raise TypedloadTypeError(str(e), type_=type_, value=value)


def _loader(in_, type_):
    """Return a typeload instance that can parse datetimes from ISO strings."""
    loader = typedload.dataloader.Loader()

    nt_handler = loader.index(datetime.datetime)
    load_handler = (
        lambda type_: type_ in {datetime.date, datetime.time, datetime.datetime},
        _datetimeload,
    )
    loader.handlers[nt_handler] = load_handler

    return loader.load(in_, type_)
