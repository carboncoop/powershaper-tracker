from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    # First Name and Last Name do not cover name patterns around the globe.
    name = models.CharField("Name of User", blank=True, max_length=255)
    USER_TYPE_CHOICES = [
        ("Financial Analyst", "Financial Analyst"),
        ("Network Analyst", "Network Analyst"),
        ("Retrofit Evaluator", "Retrofit Evaluator"),
    ]
    user_type = models.CharField(max_length=20, choices=USER_TYPE_CHOICES, blank=True)

    powershaper_token = models.TextField("PowerShaper API token", blank=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["id"]
