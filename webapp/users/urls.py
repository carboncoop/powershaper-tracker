from django.conf import settings
from django.urls import include
from django.urls import path

from . import views

if settings.USE_AUTH_SERVICE:
    urlpatterns = [
        path("", include("social_django.urls")),
        path("log-out/", views.logout, name="logout"),
        path("log-in/", views.Login.as_view(), name="login"),
        path("login-error/", views.LoginError.as_view(), name="login-error"),
    ]
else:
    urlpatterns = [
        path("", include("django.contrib.auth.urls")),
        path("register/", views.register, name="register"),
    ]
