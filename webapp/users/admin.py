from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.urls import reverse
from django.utils.html import format_html

from webapp.users.forms import UserChangeForm
from webapp.users.forms import UserCreationForm
from webapp.users.models import User


@admin.register(User)
class UserAdmin(auth_admin.UserAdmin):

    form = UserChangeForm
    add_form = UserCreationForm
    fieldsets = (
        (
            "User",
            {
                "fields": (
                    "name",
                    "user_type",
                    "powershaper_token",
                )
            },
        ),
    ) + auth_admin.UserAdmin.fieldsets
    list_display = [
        "username",
        "name",
        "email",
        "_usa",
        "is_staff",
        "is_superuser",
        "user_type",
    ]
    search_fields = ["name"]
    actions = ["username_to_email", "fill_in_full_name"]

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.prefetch_related("social_auth")

    def _usa(self, user):
        links = [
            (
                "<a href='"
                + reverse("admin:social_django_usersocialauth_change", args=(usa.id,))
                + "'>Record</a>"
            )
            for usa in user.social_auth.all()
        ]

        return format_html(", ".join(links))
