from django.conf import settings
from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth import logout as django_logout
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.shortcuts import render
from django.views.generic import TemplateView
from django.views.generic.base import RedirectView

from webapp.users import forms


def logout(request):
    django_logout(request)
    domain = settings.SOCIAL_AUTH_AUTH0_DOMAIN
    client_id = settings.SOCIAL_AUTH_AUTH0_KEY
    return_to = settings.SITE_URL
    return HttpResponseRedirect(
        f"https://{domain}/v2/logout?client_id={client_id}&returnTo={return_to}"
    )


class Login(RedirectView):
    url = settings.LOGIN_URL


class LoginError(TemplateView):
    template_name = "registration/login_error.html"


def register(request):
    form = forms.UserCreationForm()

    if request.method == "POST":
        form = forms.UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)

            user.username = user.username.lower()
            user.save()
            login(request, user)
            return redirect("home")
        else:
            messages.error(request, "An error occured during registration.")
    return render(request, "registration/register.html", {"form": form})
