import importlib

from django.core.management.base import BaseCommand

from ...locator import DATASETS
from ...models import LSOAGasElec2021
from ...models import PostcodeGasElec2021


def postcode_check():
    if PostcodeGasElec2021.objects.count() < 1:
        return True
    else:
        return False


def lsoa_check():
    if LSOAGasElec2021.objects.count() < 1:
        return True
    else:
        return False


class Command(BaseCommand):
    # See https://docs.djangoproject.com/en/2.2/howto/custom-management-commands/

    help = "Import a dataset"

    def handle(self, *args, **options):
        for model_name in DATASETS:
            module = importlib.import_module(f"webapp.datasets.sets.{model_name}")
            if hasattr(module, "load"):
                if model_name == "postcodegaselec2021" and postcode_check():
                    module.load()
                    self.stdout.write(
                        self.style.SUCCESS(
                            f"Successfully imported dataset: {model_name}"
                        )
                    )
                if model_name == "lsoagaselec2021" and lsoa_check():
                    module.load()
                    self.stdout.write(
                        self.style.SUCCESS(
                            f"Successfully imported dataset: {model_name}"
                        )
                    )
                else:
                    self.stdout.write(
                        self.style.WARNING(f"Dataset {model_name} is already loaded")
                    )

        self.stdout.write(self.style.SUCCESS("Dataset import completed"))
