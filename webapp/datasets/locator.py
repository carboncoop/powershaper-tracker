import os


def find_datasets(path):
    """List all the Python files in the given path, minus the .py extension."""

    try:
        return [f[:-3] for f in os.listdir(path) if f.endswith(".py")]
    except OSError:
        return []


path = os.path.join(os.path.dirname(__file__), "sets")
DATASETS = find_datasets(path)
