from .. import locator


def test_it_works():
    assert "postcodegaselec2020" in locator.DATASETS
    assert "postcodegaselec2021" in locator.DATASETS


def test_it_fails():
    assert locator.find_datasets("\\\\\\ThisPathDoesn'tExist") == []
