from django.db import models


class PostcodeGasElec2020(models.Model):
    postcode = models.CharField(max_length=9, primary_key=True)
    elec_mean = models.DecimalField(decimal_places=6, max_digits=12, null=True)
    gas_mean = models.DecimalField(decimal_places=6, max_digits=12, null=True)


class PostcodeGasElec2021(models.Model):
    postcode = models.CharField(max_length=9, primary_key=True)
    elec_mean = models.DecimalField(decimal_places=6, max_digits=12, null=True)
    gas_mean = models.DecimalField(decimal_places=6, max_digits=12, null=True)


class LSOAGasElec2021(models.Model):
    lsoa = models.CharField(max_length=9, primary_key=True)
    elec_mean = models.DecimalField(decimal_places=6, max_digits=12, null=True)
    gas_mean = models.DecimalField(decimal_places=6, max_digits=12, null=True)
