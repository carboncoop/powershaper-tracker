import csv
import gzip
import os
from decimal import Decimal
from io import StringIO
from pathlib import Path

from ..models import LSOAGasElec2021


files_path = Path(os.path.dirname(__file__)) / ".." / "files"


def load():  # pragma: no cover
    LSOAGasElec2021.objects.all().delete()

    with gzip.open(files_path / "LSOA_domestic_elec_2021.csv.gz") as f:
        csv_data = f.read()
    reader = csv.DictReader(StringIO(csv_data.decode("us-ascii")))
    lsoa_elec_data = {
        row["LSOA code"]: Decimal(
            row["Mean consumption (kWh per meter)"].replace(",", "")
        )
        for row in reader
        if row["Mean consumption (kWh per meter)"] != ""
    }

    with gzip.open(files_path / "LSOA_domestic_gas_2021.csv.gz") as f:
        csv_data = f.read()
    reader = csv.DictReader(StringIO(csv_data.decode("us-ascii")))
    lsoa_gas_data = {
        row["LSOA code"]: Decimal(
            row["Mean consumption (kWh per meter)"].replace(",", "")
        )
        for row in reader
        if row["Mean consumption (kWh per meter)"] != ""
    }

    all_lsoas = set(lsoa_elec_data.keys()) | set(lsoa_gas_data.keys())
    all_lsoas.remove("Unallocated")

    LSOAGasElec2021.objects.bulk_create(
        [
            LSOAGasElec2021(
                lsoa=lsoa,
                elec_mean=lsoa_elec_data.get(lsoa, None),
                gas_mean=lsoa_gas_data.get(lsoa, None),
            )
            for lsoa in all_lsoas
        ]
    )


def get(lsoa: str):
    try:
        return LSOAGasElec2021.objects.get(lsoa=lsoa)
    except LSOAGasElec2021.DoesNotExist:
        raise ValueError("Couldn't find entry for this LSOA")
