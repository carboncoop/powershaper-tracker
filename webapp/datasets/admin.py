from django.contrib import admin

from . import models

admin.site.register(models.LSOAGasElec2021)
admin.site.register(models.PostcodeGasElec2020)
admin.site.register(models.PostcodeGasElec2021)
