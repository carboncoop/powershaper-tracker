# LSOA_domestic_elec_2021 & LSOA_domestic_gas_2021

Data found at https://www.gov.uk/government/statistics/lower-and-middle-super-output-areas-electricity-consumption and https://www.gov.uk/government/statistics/lower-and-middle-super-output-areas-gas-consumption

Exported as CSV from the '2021' page of both spreadsheets using LibreOffice, and then cleaned up so that the first row contained the headings.

Pages last updated Dec 2022 at the time of fetching
