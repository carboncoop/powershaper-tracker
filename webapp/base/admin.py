from django.contrib import admin

from . import models

admin.site.register(models.AnalysisJob)
admin.site.register(models.MeterData)
admin.site.register(models.JobResults)
