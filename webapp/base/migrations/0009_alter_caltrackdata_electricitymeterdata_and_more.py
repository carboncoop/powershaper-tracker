# Generated by Django 4.1.2 on 2022-10-14 13:57
import datetime

from django.db import migrations
from django.db import models


class Migration(migrations.Migration):

    dependencies = [
        ("base", "0008_alter_caltrackdata_electricitymeterdata_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="caltrackdata",
            name="electricityMeterData",
            field=models.FileField(upload_to=""),
        ),
        migrations.AlterField(
            model_name="caltrackdata",
            name="gasMeterData",
            field=models.FileField(upload_to=""),
        ),
        migrations.AlterField(
            model_name="caltrackdata",
            name="postcode",
            field=models.CharField(max_length=8),
        ),
        migrations.AlterField(
            model_name="caltrackdata",
            name="retrofitEndDate",
            field=models.DateField(
                default=datetime.date.today, verbose_name="startDate"
            ),
        ),
        migrations.AlterField(
            model_name="caltrackdata",
            name="retrofitStartDate",
            field=models.DateField(
                default=datetime.date.today, verbose_name="startDate"
            ),
        ),
    ]
