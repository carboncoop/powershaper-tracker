# Generated by Django 4.0.5 on 2022-11-24 11:25
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("base", "0017_remove_tempdata_id_alter_tempdata_year"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="caltrackdata",
            name="user_type",
        ),
    ]
