# Generated by Django 4.0.5 on 2023-02-10 16:01
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("base", "0034_add_csv_download_infra"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="caltrackdata",
            options={"ordering": ["-created_at"]},
        ),
    ]
