# Generated by Django 4.0.5 on 2023-02-21 11:28
from django.db import migrations
from django.db import models


class Migration(migrations.Migration):

    dependencies = [
        ("base", "0042_add_all_date_ranges"),
    ]

    operations = [
        migrations.AddField(
            model_name="caltrackdata",
            name="status",
            field=models.CharField(
                choices=[
                    ("NOT_STARTED", "not started"),
                    ("IN_PROGRESS", "in progress"),
                    ("COMPLETE", "complete"),
                ],
                default="NOT_STARTED",
                max_length=12,
            ),
        ),
    ]
