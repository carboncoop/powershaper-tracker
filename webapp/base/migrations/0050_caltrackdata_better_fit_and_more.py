# Generated by Django 4.1.7 on 2023-02-27 12:10
from django.db import migrations
from django.db import models


class Migration(migrations.Migration):

    dependencies = [
        ("base", "0049_add_baseline_fields"),
    ]

    operations = [
        migrations.AddField(
            model_name="caltrackdata",
            name="better_fit",
            field=models.CharField(blank=True, max_length=10, null=True),
        ),
        migrations.AddField(
            model_name="caltrackdata",
            name="electricity_cvrmse_daily",
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="caltrackdata",
            name="electricity_cvrmse_hourly",
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="caltrackdata",
            name="electricity_metered_savings_daily_from_hourly",
            field=models.JSONField(blank=True, default=dict),
        ),
        migrations.AddField(
            model_name="caltrackdata",
            name="electricity_metered_savings_monthly_from_daily",
            field=models.JSONField(blank=True, default=dict),
        ),
        migrations.AddField(
            model_name="caltrackdata",
            name="electricity_nmbe_daily",
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="caltrackdata",
            name="electricity_nmbe_hourly",
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="caltrackdata",
            name="electricity_percent_savings_daily",
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="caltrackdata",
            name="electricity_rsquared_daily",
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="caltrackdata",
            name="electricity_rsquared_hourly",
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="caltrackdata",
            name="electricity_total_savings_daily",
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="caltrackdata",
            name="gas_cvrmse_daily",
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="caltrackdata",
            name="gas_cvrmse_hourly",
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="caltrackdata",
            name="gas_metered_savings_daily_from_hourly",
            field=models.JSONField(blank=True, default=dict),
        ),
        migrations.AddField(
            model_name="caltrackdata",
            name="gas_metered_savings_monthly_from_daily",
            field=models.JSONField(blank=True, default=dict),
        ),
        migrations.AddField(
            model_name="caltrackdata",
            name="gas_nmbe_daily",
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="caltrackdata",
            name="gas_nmbe_hourly",
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="caltrackdata",
            name="gas_percent_savings_daily",
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="caltrackdata",
            name="gas_rsquared_daily",
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="caltrackdata",
            name="gas_rsquared_hourly",
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="caltrackdata",
            name="gas_total_savings_daily",
            field=models.FloatField(blank=True, null=True),
        ),
    ]
