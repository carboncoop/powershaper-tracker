import datetime
import io
from dataclasses import dataclass

import eemeter
import numpy as np
import pandas as pd
from dateutil.relativedelta import relativedelta

from . import enums
from . import models
from .models import MeteredSavingsDaily
from .models import MeteredSavingsHourly
from .temp_data import get_weather_intl
from webapp.apis import meters as psm_api
from webapp.apis import postcodes_io as postcodes_api


def fetch_lsoa(job: models.AnalysisJob):
    try:
        job.lsoa = postcodes_api.get_lsoa_for_postcode(job.postcode)
    except LookupError:
        pass


def fill_data_from_powershaper(
    job: models.AnalysisJob,
    type_: enums.MeterType,
):
    def _get_meterdata_for_uuid_type_pair(uuid):
        records = psm_api.get_meter_records(
            meter_uuid=uuid,
            type=type_,
            token=job.user.powershaper_token,
            start=job.baseline_start_date,
            end=job.reporting_end_date,
        )

        data = [(record.time.isoformat(), record.energy_kwh) for record in records]

        # We rely on the server giving us an ordered list
        earliest_timestamp = records[0].time
        latest_timestamp = records[-1].time

        return models.MeterData(
            job=job,
            type=type_,
            earliest=earliest_timestamp,
            latest=latest_timestamp,
            data=data,
        )

    return [_get_meterdata_for_uuid_type_pair(uuid) for uuid in job.psm_consents]


def first_full_day_from(dt: datetime.datetime) -> datetime.date:
    if dt.hour == 0 and dt.min == 0 and dt.second == 0 and dt.microsecond == 0:
        return dt
    else:
        return dt.date() + datetime.timedelta(days=1)


def last_complete_day_before(dt: datetime.datetime) -> datetime:
    if dt.hour == 0 and dt.min == 0 and dt.second == 0 and dt.microsecond == 0:
        return dt
    else:
        return dt.date() - datetime.timedelta(days=1)


def get_date_range(job):
    if job.data_origin == enums.DataOrigin.PS_MONITOR:
        return (job.psm_range_start, job.psm_range_end)
    else:
        # Find the largest date range that contains all meter data.
        dataset = job.meterdata_set.all()
        earliest = max(first_full_day_from(meterdata.earliest) for meterdata in dataset)
        latest = min(
            last_complete_day_before(meterdata.latest) for meterdata in dataset
        )
        return (earliest, latest)


def set_dates_based_on_intervention_period(
    job: models.AnalysisJob,
    intervention_start: datetime.date,
    intervention_end: datetime.date,
):
    job.intervention_start_date = intervention_start
    job.intervention_end_date = intervention_end
    job.baseline_end_date = intervention_start - relativedelta(days=1)
    # 365 is the maximum to conform to CalTrack
    # https://eemeter.readthedocs.io/api.html?highlight=get_reporting_data#eemeter.get_baseline_data
    job.baseline_start_date = job.baseline_end_date - relativedelta(days=364)
    job.reporting_start_date = job.intervention_end_date + relativedelta(days=1)
    # 5 full days before today because that's when the CDS API has weather data for
    job.reporting_end_date = min(
        get_date_range(job)[1],
        datetime.date.today() - relativedelta(days=6),
    )


def convert_json_to_df_data(json):
    meter1 = pd.DataFrame(json)
    meter1 = meter1.drop(["carbon_kg"], axis=1)
    meter = meter1.rename(
        columns={"time": "date", "energy_kwh": "value", "error_count": "error"}
    )

    return meter


def format_meter_data(meter_data1):
    meter_data1.index = pd.to_datetime(meter_data1.index)
    meter_data = meter_data1.sort_index()
    meter_data.index.name = "start"
    meter_data_hourly = meter_data.drop(["error"], axis=1)

    del meter_data1
    del meter_data

    if meter_data_hourly.index[0].minute != 00:
        meter_data_hourly = meter_data_hourly.iloc[1:, :]
    if meter_data_hourly.index[-1].minute != 00:
        meter_data_hourly = meter_data_hourly.iloc[:-1, :]

    meter_data_hourly = meter_data_hourly.resample("H").sum()

    return meter_data_hourly


def fetch_temperature_data(job):
    """Fetch temperature for a given job."""

    date_range = _date_range_to_datetime_range(
        job.baseline_start_date, job.reporting_end_date
    )
    weather = get_weather_intl(
        start_date=date_range.start,
        end_date=date_range.end,
        areacode=job.postcode,
    )
    weather.index = pd.to_datetime(weather.index)

    return weather.squeeze().asfreq(freq="H").tz_localize("UTC").rename("tempF")


@dataclass
class DateTimeRange:
    """A datetime range, inclusive at both ends."""

    start: datetime.datetime
    end: datetime.datetime


def _date_range_to_datetime_range(
    start_date: datetime.date,
    end_date: datetime.date,
) -> DateTimeRange:
    """Calculate the earliest and latest possible timestamps in a range of days."""
    return DateTimeRange(
        start=datetime.datetime(
            start_date.year,
            start_date.month,
            start_date.day,
            tzinfo=datetime.timezone.utc,
        ),
        end=datetime.datetime(
            end_date.year,
            end_date.month,
            end_date.day,
            datetime.time.max.hour,
            datetime.time.max.minute,
            datetime.time.max.second,
            datetime.time.max.microsecond,
            tzinfo=datetime.timezone.utc,
        ),
    )


def run_caltrack_model(
    metered_data: pd.DataFrame,
    type: str,  # gas or electricity
    meter_data_hourly: pd.DataFrame,
):
    fake_file = io.StringIO(metered_data.temperature_data)
    hourly_temperatures = pd.read_csv(fake_file, index_col=0)
    hourly_temperatures.index = pd.to_datetime(hourly_temperatures.index)
    hourly_temperatures = hourly_temperatures.squeeze().asfreq(freq="H")

    # create daily meter data from hourly
    meter_data_daily = meter_data_hourly.resample("D").sum()

    # hourly caltrack model
    baseline_period = _date_range_to_datetime_range(
        metered_data.baseline_start_date, metered_data.baseline_end_date
    )
    baseline_meter_data_hourly, baseline_warnings_hourly = eemeter.get_baseline_data(
        data=meter_data_hourly,
        start=baseline_period.start,
        end=baseline_period.end,
        max_days=None,
    )

    preliminary_design_matrix_hourly = (
        eemeter.create_caltrack_hourly_preliminary_design_matrix(
            baseline_meter_data_hourly,
            hourly_temperatures,
        )
    )

    segmentation_hourly = eemeter.segment_time_series(
        preliminary_design_matrix_hourly.index, "three_month_weighted"
    )

    occupancy_lookup_hourly = eemeter.estimate_hour_of_week_occupancy(
        preliminary_design_matrix_hourly,
        segmentation=segmentation_hourly,
        # threshold=0.65  # default
    )

    (
        temperature_bins_hourly_occupied,
        temperature_bins_hourly_unoccupied,
    ) = eemeter.fit_temperature_bins(
        preliminary_design_matrix_hourly,
        segmentation=segmentation_hourly,
        occupancy_lookup=occupancy_lookup_hourly,
        # default_bins=[30, 45, 55, 65, 75, 90],  # default
        # min_temperature_count=20  # default
    )

    segmented_design_matrices_hourly = (
        eemeter.create_caltrack_hourly_segmented_design_matrices(
            preliminary_design_matrix_hourly,
            segmentation_hourly,
            occupancy_lookup_hourly,
            temperature_bins_hourly_occupied,
            temperature_bins_hourly_unoccupied,
        )
    )

    baseline_segmented_model_hourly = eemeter.fit_caltrack_hourly_model(
        segmented_design_matrices_hourly,
        occupancy_lookup_hourly,
        temperature_bins_hourly_occupied,
        temperature_bins_hourly_unoccupied,
    )

    reporting_period = _date_range_to_datetime_range(
        metered_data.reporting_start_date, metered_data.reporting_end_date
    )

    reporting_meter_data_hourly, warnings = eemeter.get_reporting_data(
        meter_data_hourly,
        start=reporting_period.start,
        end=reporting_period.end,
        max_days=None,
    )

    metered_savings_hourly, error_bands = eemeter.metered_savings(
        baseline_segmented_model_hourly,
        reporting_meter_data_hourly,
        hourly_temperatures,
    )

    metered_savings_hourly_baseline, error_bands = eemeter.metered_savings(
        baseline_segmented_model_hourly,
        baseline_meter_data_hourly,
        hourly_temperatures,
    )

    # daily caltrack model
    baseline_meter_data_daily, baseline_warnings_daily = eemeter.get_baseline_data(
        data=meter_data_daily,
        start=baseline_period.start,
        end=baseline_period.end,
        max_days=None,
    )

    # helper function for clean baseline data
    baseline_meter_data_daily = eemeter.clean_caltrack_billing_daily_data(
        baseline_meter_data_daily, "daily"
    )

    design_matrix_daily = eemeter.create_caltrack_daily_design_matrix(
        baseline_meter_data_daily,
        hourly_temperatures,
    )

    baseline_model_results_daily = eemeter.fit_caltrack_usage_per_day_model(
        data=design_matrix_daily, fit_cdd=False if type == "gas" else True
    )

    reporting_meter_data_daily, warnings = eemeter.get_reporting_data(
        meter_data_daily,
        start=reporting_period.start,
        end=reporting_period.end,
        max_days=None,
    )

    metered_savings_daily, error_bands = eemeter.metered_savings(
        baseline_model_results_daily,
        reporting_meter_data_daily,
        hourly_temperatures,
        with_disaggregated=True,
    )

    metered_savings_daily_baseline, error_bands = eemeter.metered_savings(
        baseline_model_results_daily,
        baseline_meter_data_daily,
        hourly_temperatures,
        with_disaggregated=True,
    )

    # saving hourly records to db
    savings_records = metered_savings_hourly.to_dict("index")
    # Create instances of MeteredSavingsHourly from hourly dict
    savings_instances = [
        MeteredSavingsHourly(
            caltrack=metered_data,
            start=key,
            counterfactual_usage=value["counterfactual_usage"],
            reporting_observed=value["reporting_observed"],
            metered_savings=value["metered_savings"],
            type=type,
        )
        for key, value in savings_records.items()
    ]
    # saving hourly records to db
    savings_records_daily = metered_savings_daily.to_dict("index")

    # Create instances of MeteredSavingsHourly from hourly dict
    savings_instances_daily = [
        MeteredSavingsDaily(
            caltrack=metered_data,
            start=key,
            counterfactual_usage=value["counterfactual_usage"],
            reporting_observed=value["reporting_observed"],
            metered_savings=value["metered_savings"],
            type=type,
        )
        for key, value in savings_records_daily.items()
    ]

    # Only perform the bulk creation if we have a primary key (for testing)
    # TODO(anna): refactor
    if metered_data.pk:
        MeteredSavingsHourly.objects.bulk_create(savings_instances)
        MeteredSavingsDaily.objects.bulk_create(savings_instances_daily)

    columns = ["reporting_observed", "counterfactual_usage", "metered_savings"]

    df5 = baseline_meter_data_hourly.rename(columns={"value": "reporting_observed"})
    new_columns = [
        "counterfactual_base_load",
        "counterfactual_cooling_load",
        "counterfactual_heating_load",
        "counterfactual_usage",
        "metered_savings",
    ]
    df5 = df5.iloc[:-1]
    df5[new_columns] = 0
    df6 = df5.combine_first(metered_savings_hourly)
    df6 = df6

    daily_baseline_savings_hourly = [
        {
            "date": date.strftime("%Y-%m-%d"),
            "observed": results["reporting_observed"],
            "counterfactual": results["counterfactual_usage"],
            "saving": results["metered_savings"],
        }
        for date, results in df6[columns].resample("D").sum().iterrows()
    ]

    df2 = baseline_meter_data_daily.rename(columns={"value": "reporting_observed"})
    # Add new columns with initial values of 0
    df2 = df2.iloc[:-1]
    df2[new_columns] = 0
    df3 = df2.combine_first(metered_savings_daily)

    daily_baseline_savings_daily = [
        {
            "date": date.strftime("%Y-%m-%d"),
            "observed": results["reporting_observed"],
            "counterfactual": results["counterfactual_usage"],
            "saving": results["metered_savings"],
        }
        for date, results in df3[columns].iloc[:-1].iterrows()
    ]

    cvrmse_hourly = eemeter.ModelMetrics(
        metered_savings_hourly_baseline["reporting_observed"],
        metered_savings_hourly_baseline["counterfactual_usage"],
    ).cvrmse
    nmbe_hourly = eemeter.ModelMetrics(
        metered_savings_hourly_baseline["reporting_observed"],
        metered_savings_hourly_baseline["counterfactual_usage"],
    ).nmbe
    rsquared_hourly = eemeter.ModelMetrics(
        metered_savings_hourly_baseline["reporting_observed"],
        metered_savings_hourly_baseline["counterfactual_usage"],
    ).r_squared

    cvrmse_daily = eemeter.ModelMetrics(
        metered_savings_daily_baseline["reporting_observed"],
        metered_savings_daily_baseline["counterfactual_usage"],
    ).cvrmse
    nmbe_daily = eemeter.ModelMetrics(
        metered_savings_daily_baseline["reporting_observed"],
        metered_savings_daily_baseline["counterfactual_usage"],
    ).nmbe
    rsquared_daily = eemeter.ModelMetrics(
        metered_savings_daily_baseline["reporting_observed"],
        metered_savings_daily_baseline["counterfactual_usage"],
    ).r_squared

    better_fit = None

    def closest_value(input_list, input_value):
        arr = np.asarray(input_list)
        i = (np.abs(arr - input_value)).argmin()
        return arr[i]

    value = closest_value([cvrmse_hourly, cvrmse_daily], 0)

    if cvrmse_hourly == value:
        better_fit = enums.ModelType.HOURLY
    else:
        better_fit = enums.ModelType.DAILY

    total_savings_hourly = metered_savings_hourly.metered_savings.sum()
    percent_savings_hourly = (
        total_savings_hourly / metered_savings_hourly.counterfactual_usage.sum() * 100
    )

    total_savings_daily = metered_savings_daily.metered_savings.sum()
    percent_savings_daily = (
        total_savings_daily / metered_savings_daily.counterfactual_usage.sum() * 100
    )

    return (
        better_fit,
        daily_baseline_savings_hourly,
        total_savings_hourly,
        percent_savings_hourly,
        cvrmse_hourly,
        nmbe_hourly,
        rsquared_hourly,
        daily_baseline_savings_daily,
        total_savings_daily,
        percent_savings_daily,
        cvrmse_daily,
        nmbe_daily,
        rsquared_daily,
    )
