from django.contrib.postgres.fields import ArrayField
from django.db import models

from . import enums
from webapp.users.models import User


class AnalysisJob(models.Model):
    class Meta:
        ordering = ["-created_at"]

    # Stage 1
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    portfolio_mode = models.CharField(
        choices=enums.PortfolioMode.choices, max_length=30
    )
    data_origin = models.CharField(choices=enums.DataOrigin.choices, max_length=30)
    analyse_gas = models.BooleanField(blank=True)
    analyse_elec = models.BooleanField(blank=True)

    # Stage 2
    psm_consents = ArrayField(models.CharField(max_length=40), blank=True, default=list)
    psm_range_start = models.DateField(null=True, blank=True)
    psm_range_end = models.DateField(null=True, blank=True)

    # Stage 3
    postcode = models.CharField(blank=True, max_length=8)
    reference = models.TextField(blank=True)
    floor_area = models.FloatField(null=True, blank=True)
    lsoa = models.CharField(blank=True, max_length=10)
    baseline_start_date = models.DateField(null=True, blank=True)
    baseline_end_date = models.DateField(null=True, blank=True)
    intervention_start_date = models.DateField(null=True, blank=True)
    intervention_end_date = models.DateField(null=True, blank=True)
    reporting_start_date = models.DateField(null=True, blank=True)
    reporting_end_date = models.DateField(null=True, blank=True)

    # Processing phase
    temperature_data = models.TextField(blank=True)
    electricity_baseline_usage = models.FloatField(null=True, blank=True)
    gas_baseline_usage = models.FloatField(null=True, blank=True)
    better_fit = models.CharField(
        choices=enums.ModelType.choices, blank=True, null=True, max_length=30
    )

    # Timestamps
    created_at = models.DateTimeField(auto_now_add=True)
    submitted_at = models.DateTimeField(null=True, blank=True)
    processing_started_at = models.DateTimeField(null=True, blank=True)
    processing_finished_at = models.DateTimeField(null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True)
    failed_at = models.DateTimeField(null=True, blank=True)

    @property
    def status(self):
        if self.failed_at and not self.processing_finished_at:
            return "failed"
        if not self.submitted_at:
            return "incomplete"
        if not self.processing_started_at:
            return "not started"
        if not self.processing_finished_at:
            return "in progress"
        return "complete"


class MeterData(models.Model):
    job = models.ForeignKey(AnalysisJob, on_delete=models.CASCADE)
    type = models.CharField(choices=enums.MeterType.choices, max_length=30)
    earliest = models.DateTimeField()
    latest = models.DateTimeField()
    data = models.JSONField()


class JobResults(models.Model):
    job = models.ForeignKey(AnalysisJob, on_delete=models.CASCADE)
    model_type = models.CharField(choices=enums.ModelType.choices, max_length=30)
    meter_type = models.CharField(choices=enums.MeterType.choices, max_length=30)

    total_savings = models.FloatField(null=True, blank=True)
    percent_savings = models.FloatField(null=True, blank=True)
    results = models.JSONField(blank=True, default=list)

    cvrmse = models.FloatField(null=True, blank=True)
    nmbe = models.FloatField(null=True, blank=True)
    rsquared = models.FloatField(null=True, blank=True)


class MeteredSavingsHourly(models.Model):
    caltrack = models.ForeignKey(AnalysisJob, on_delete=models.CASCADE, null=True)
    start = models.DateTimeField(auto_now=False, auto_now_add=False, null=True)
    reporting_observed = models.FloatField()
    counterfactual_usage = models.FloatField()
    metered_savings = models.FloatField()

    # TODO(anna): replace with TextChoices
    gas = "Gas"
    electricity = "Electricity"
    type = [(electricity, "Electricity"), (gas, "Gas")]

    type = models.CharField(max_length=16, choices=type, null=True)


class MeteredSavingsDaily(models.Model):
    caltrack = models.ForeignKey(AnalysisJob, on_delete=models.CASCADE, null=True)
    start = models.DateTimeField(auto_now=False, auto_now_add=False, null=True)
    reporting_observed = models.FloatField()
    counterfactual_usage = models.FloatField()
    metered_savings = models.FloatField()

    # TODO(anna): replace with TextChoices
    gas = "Gas"
    electricity = "Electricity"
    type = [(electricity, "Electricity"), (gas, "Gas")]

    type = models.CharField(max_length=16, choices=type, null=True)
