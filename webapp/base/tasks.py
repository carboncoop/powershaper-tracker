from functools import reduce

import pandas as pd
from django.utils import timezone
from django_rq import job
from rq import Retry

from . import enums
from . import models
from . import services


@job("default", retry=Retry(max=2))
def fetch_powershaper_data(job_id):
    """Fetch meter data from PowerShaper Monitor if required."""
    job = models.AnalysisJob.objects.get(pk=job_id)
    job.processing_started_at = timezone.now()
    job.save()

    if job.data_origin == enums.DataOrigin.PS_MONITOR:
        if job.analyse_elec:
            result = services.fill_data_from_powershaper(job, enums.MeterType.ELEC)
            models.MeterData.objects.bulk_create(result)

        if job.analyse_gas:
            result = services.fill_data_from_powershaper(job, enums.MeterType.GAS)
            models.MeterData.objects.bulk_create(result)

    fetch_temperature.delay(job_id, job_id=f"fetch_temp_{job_id}")


@job("default", retry=Retry(max=2))
def fetch_temperature(job_id):
    """Fetch temperature data and store it for use by the next jobs."""

    job = models.AnalysisJob.objects.get(pk=job_id)
    job.temperature_data = services.fetch_temperature_data(job).to_csv()
    job.save()

    if job.analyse_elec:
        calculate_elec_savings.delay(job_id, job_id=f"calc_elec_{job_id}")
    if job.analyse_gas:
        calculate_gas_savings.delay(job_id, job_id=f"calc_gas_{job_id}")


def _get_baseline_usage(df, baseline_start_date, baseline_end_date):
    """Sum values from a given period within a time-indexed dataframe."""
    daily_df = df.resample("D").sum()
    dt_range = services._date_range_to_datetime_range(
        baseline_start_date, baseline_end_date
    )
    subset_df = daily_df.loc[dt_range.start : dt_range.end]
    assert len(subset_df) == 365
    return subset_df.value.sum()


def _is_complete(job):
    """Work out whether a job is complete or not."""
    if (
        job.analyse_gas
        and job.jobresults_set.filter(meter_type=enums.MeterType.GAS).count() == 0
    ):
        return False

    if (
        job.analyse_elec
        and job.jobresults_set.filter(meter_type=enums.MeterType.ELEC).count() == 0
    ):
        return False

    return True


def _make_df_for_meterdata(meterdata: models.MeterData):
    """Return a dataframe containing the data in a MeterData record."""
    result = pd.DataFrame.from_records(
        meterdata.data, columns=["timestamp", "value"], index="timestamp"
    )
    result.index = pd.to_datetime(result.index)
    result = result.resample("H").sum()

    # TODO(anna): drop any unneeded data points here

    return result


def _get_totalled_data(job, meter_type):
    """
    Return a dataframe containing aggregated data for a whole job.

    If we're only analysing one meter's data, then this just returns the data from that
    meter. Otherwise, it sums the different sets of data we have.
    """
    meterdatas = job.meterdata_set.filter(type=meter_type)
    dfs = [_make_df_for_meterdata(meterdata) for meterdata in meterdatas]

    if len(dfs) == 1:
        return dfs[0]
    else:
        total_df = dfs[0].copy()
        reduce(lambda a, b: a.add(b, fill_value=0), [total_df, *dfs[1:]])
        return total_df


@job("default", retry=Retry(max=2))
def calculate_elec_savings(job_id):
    job = models.AnalysisJob.objects.get(pk=job_id)
    df = _get_totalled_data(job, enums.MeterType.ELEC)
    job.electricity_baseline_usage = _get_baseline_usage(
        df, job.baseline_start_date, job.baseline_end_date
    )

    (
        better_fit,
        daily_savings_from_hourly,
        total_savings_from_hourly,
        percent_savings_from_hourly,
        cvrmse_hourly,
        nmbe_hourly,
        rsquared_hourly,
        daily_savings_from_daily,
        total_savings_daily,
        percent_savings_daily,
        cvrmse_daily,
        nmbe_daily,
        rsquared_daily,
    ) = services.run_caltrack_model(job, "Electricity", df)

    job.better_fit = better_fit

    models.JobResults.objects.create(
        job=job,
        model_type=enums.ModelType.DAILY,
        meter_type=enums.MeterType.ELEC,
        total_savings=total_savings_daily,
        percent_savings=percent_savings_daily,
        results=daily_savings_from_daily,
        cvrmse=cvrmse_daily,
        nmbe=nmbe_daily,
        rsquared=rsquared_daily,
    )

    models.JobResults.objects.create(
        job=job,
        model_type=enums.ModelType.HOURLY,
        meter_type=enums.MeterType.ELEC,
        total_savings=total_savings_from_hourly,
        percent_savings=percent_savings_from_hourly,
        results=daily_savings_from_hourly,
        cvrmse=cvrmse_hourly,
        nmbe=nmbe_hourly,
        rsquared=rsquared_hourly,
    )

    if _is_complete(job):
        job.processing_finished_at = timezone.now()

    job.save()


@job("default", retry=Retry(max=2))
def calculate_gas_savings(job_id):
    job = models.AnalysisJob.objects.get(pk=job_id)
    df = _get_totalled_data(job, enums.MeterType.GAS)
    job.gas_baseline_usage = _get_baseline_usage(
        df, job.baseline_start_date, job.baseline_end_date
    )

    (
        better_fit,
        daily_savings_from_hourly,
        total_savings_from_hourly,
        percent_savings_from_hourly,
        cvrmse_hourly,
        nmbe_hourly,
        rsquared_hourly,
        daily_savings_from_daily,
        total_savings_daily,
        percent_savings_daily,
        cvrmse_daily,
        nmbe_daily,
        rsquared_daily,
    ) = services.run_caltrack_model(job, "Gas", df)

    job.better_fit = better_fit

    models.JobResults.objects.create(
        job=job,
        model_type=enums.ModelType.DAILY,
        meter_type=enums.MeterType.GAS,
        total_savings=total_savings_daily,
        percent_savings=percent_savings_daily,
        results=daily_savings_from_daily,
        cvrmse=cvrmse_daily,
        nmbe=nmbe_daily,
        rsquared=rsquared_daily,
    )

    models.JobResults.objects.create(
        job=job,
        model_type=enums.ModelType.HOURLY,
        meter_type=enums.MeterType.GAS,
        total_savings=total_savings_from_hourly,
        percent_savings=percent_savings_from_hourly,
        results=daily_savings_from_hourly,
        cvrmse=cvrmse_hourly,
        nmbe=nmbe_hourly,
        rsquared=rsquared_hourly,
    )

    if _is_complete(job):
        job.processing_finished_at = timezone.now()

    job.save()


def custom_handler(job, *exc_info):
    if not job.retries_left:
        dbjob = models.AnalysisJob.objects.get(pk=job.args[0])
        dbjob.failed_at = timezone.now()
        dbjob.save()
