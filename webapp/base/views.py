import csv
import logging

import pandas as pd
from dateutil.relativedelta import relativedelta
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.views.generic import FormView

from . import enums
from . import forms
from . import models
from .models import AnalysisJob
from .services import _date_range_to_datetime_range
from webapp.datasets.sets import lsoagaselec2021
from webapp.datasets.sets import postcodegaselec2021

logger = logging.getLogger(__name__)


# SAP10
GRAMS_CO_PER_KWH_ELECTRICITY = 136
GRAMS_CO_PER_KWH_GAS = 210


def home(request):
    return render(request, "base/home.html")


class Settings(LoginRequiredMixin, FormView):
    template_name = "base/settings.html"
    form_class = forms.TokenForm

    def get_initial(self):
        return {"token": self.request.user.powershaper_token}

    def get_context_data(self):
        context = super().get_context_data()
        context["valid_key"] = self.request.user.powershaper_token != ""
        return context

    def form_valid(self, form):
        self.request.user.powershaper_token = form.cleaned_data["token"]
        self.request.user.save()

        return self.render_to_response(self.get_context_data())


@login_required
def history(request):
    # TODO(anna): make sure we only fetch the columns we need
    data = AnalysisJob.objects.filter(user=request.user).exclude(submitted_at=None)
    p = Paginator(data, 20)
    page = request.GET.get("page")
    results = p.get_page(page)

    def _get_pct_saving(job, meter_type):
        try:
            result = job.jobresults_set.get(
                model_type=job.better_fit, meter_type=meter_type
            )
        except models.JobResults.DoesNotExist:
            return None

        return result.percent_savings

    context = {
        "jobs": [
            {
                "pk": job.pk,
                "ref": job.reference,
                "postcode": job.postcode,
                "status": job.status,
                "gas_saving": _get_pct_saving(job, enums.MeterType.GAS),
                "elec_saving": _get_pct_saving(job, enums.MeterType.ELEC),
                "submitted_at": job.submitted_at.date(),
            }
            for job in results
        ]
    }
    return render(request, "base/history.html", context)


def _date_to_dict(value):
    parts = value.split("-")
    return {"day": int(parts[2]), "month": int(parts[1]), "year": int(parts[0])}


def _output_for_if(
    job,
    model_type,
):
    try:
        elec_result = job.jobresults_set.get(
            model_type=model_type, meter_type=enums.MeterType.ELEC
        )
    except models.JobResults.DoesNotExist:
        return None
    try:
        gas_result = job.jobresults_set.get(
            model_type=model_type, meter_type=enums.MeterType.GAS
        )
    except models.JobResults.DoesNotExist:
        return None

    elec_daily_df = pd.DataFrame(elec_result.results)
    elec_daily_df["date"] = pd.to_datetime(elec_daily_df["date"], format="%Y-%m-%d")
    elec_daily_df = elec_daily_df.set_index(["date"])

    elec_reporting_period_at_least_365_days = (
        job.reporting_start_date + relativedelta(days=364) <= job.reporting_end_date
    )

    gas_daily_df = pd.DataFrame(gas_result.results)
    gas_daily_df["date"] = pd.to_datetime(gas_daily_df["date"], format="%Y-%m-%d")
    gas_daily_df = gas_daily_df.set_index(["date"])

    gas_reporting_period_at_least_365_days = (
        job.reporting_start_date + relativedelta(days=364) <= job.reporting_end_date
    )

    if (
        elec_reporting_period_at_least_365_days
        and gas_reporting_period_at_least_365_days
        and job.processing_finished_at
        and job.floor_area
    ):
        start_plus_364_days = job.reporting_start_date + relativedelta(days=364)

        elec_subset_df = elec_daily_df.loc[
            job.reporting_start_date : start_plus_364_days
        ]
        gas_subset_df = gas_daily_df.loc[job.reporting_start_date : start_plus_364_days]
        assert len(elec_subset_df) == 365
        assert len(gas_subset_df) == 365

        elec_counterfactual_usage_first_365_days = elec_subset_df.counterfactual.sum()
        elec_reporting_first_365_days_usage = elec_subset_df.observed.sum()
        gas_counterfactual_usage_first_365_days = gas_subset_df.counterfactual.sum()
        gas_reporting_first_365_days_usage = gas_subset_df.observed.sum()

        post_intervention = (
            elec_reporting_first_365_days_usage + gas_reporting_first_365_days_usage
        ) / job.floor_area
        pre_intervention = (
            elec_counterfactual_usage_first_365_days
            + gas_counterfactual_usage_first_365_days
        ) / job.floor_area
        co2_sum = (
            (
                elec_reporting_first_365_days_usage
                * (GRAMS_CO_PER_KWH_ELECTRICITY / 1000)
            )
            + (gas_reporting_first_365_days_usage * (GRAMS_CO_PER_KWH_GAS / 1000))
        ) / job.floor_area
    else:
        post_intervention = None
        pre_intervention = None
        co2_sum = None

    return {
        "post_intervention": post_intervention if post_intervention else None,
        "pre_intervention": pre_intervention if pre_intervention else None,
        "co2_sum": co2_sum if co2_sum else None,
    }


def _output_for(job, model_type, meter_type):
    try:
        result = job.jobresults_set.get(model_type=model_type, meter_type=meter_type)
    except models.JobResults.DoesNotExist:
        return None

    daily_df = pd.DataFrame(result.results)
    daily_df["date"] = pd.to_datetime(daily_df["date"], format="%Y-%m-%d")
    daily_df = daily_df.set_index(["date"])

    reporting_period_at_least_365_days = (
        job.reporting_start_date + relativedelta(days=364) <= job.reporting_end_date
    )
    if job.processing_finished_at:
        job_reporting_date_range = _date_range_to_datetime_range(
            job.reporting_start_date, job.reporting_end_date
        )

        report_subset_df = daily_df.loc[
            job_reporting_date_range.start.replace(
                tzinfo=None
            ) : job_reporting_date_range.end.replace(tzinfo=None)
        ]

        counterfactual_usage_total_reporting_period = (
            report_subset_df.counterfactual.sum()
        )
        reporting_first_total_reporting_period = report_subset_df.observed.sum()

        if meter_type == enums.MeterType.ELEC:
            co2_saving_kg = int(
                (
                    counterfactual_usage_total_reporting_period
                    - reporting_first_total_reporting_period
                )
                * (GRAMS_CO_PER_KWH_ELECTRICITY / 1000)
            )
        if meter_type == enums.MeterType.GAS:
            co2_saving_kg = int(
                (
                    counterfactual_usage_total_reporting_period
                    - reporting_first_total_reporting_period
                )
                * (GRAMS_CO_PER_KWH_GAS / 1000)
            )
    if reporting_period_at_least_365_days and job.processing_finished_at:
        start_plus_364_days = job.reporting_start_date + relativedelta(days=364)
        subset_df = daily_df.loc[job.reporting_start_date : start_plus_364_days]
        assert len(subset_df) == 365

        reporting_first_365_days_usage = subset_df.observed.sum()
    else:
        reporting_first_365_days_usage = None

    return {
        "reduction": result.total_savings,
        "reduction_pct": result.percent_savings,
        "cvrmse": result.cvrmse,
        "nmbe": result.nmbe,
        "rsquared": result.rsquared,
        "annual_usage_kwh": reporting_first_365_days_usage,
        "co2_saving_kg": co2_saving_kg,
        "daily_figures": [
            {
                "date": _date_to_dict(row["date"]),
                "observed": row["observed"],
                "counterfactual": row["counterfactual"],
                "savings": row["saving"],
            }
            for row in result.results
        ],
        "monthly_figures": [
            {
                "date": {"month": timestamp.month, "year": timestamp.year},
                "observed": values["observed"],
                "counterfactual": values["counterfactual"],
                "savings": values["saving"],
            }
            for timestamp, values in daily_df.resample("MS").sum().iterrows()
        ],
        "cumulative_figures": [
            {
                "date": {"month": timestamp.month, "year": timestamp.year},
                "observed": values["observed"],
                "counterfactual": values["counterfactual"],
                "savings": values["saving"],
            }
            for timestamp, values in daily_df.cumsum().iterrows()
        ],
    }


@login_required
def result(request, pk):
    job = get_object_or_404(AnalysisJob, user=request.user, pk=pk)

    try:
        postcode_average = postcodegaselec2021.get(job.postcode)
    except ValueError:
        postcode_average = None

    try:
        lsoa_average = lsoagaselec2021.get(job.lsoa)
    except ValueError:
        lsoa_average = None

    context = {
        "job": {
            "id": job.pk,
            "submitted_at": job.created_at,
            "started_at": job.processing_started_at,
            "finished_at": job.processing_finished_at,
            "floor_area": job.floor_area,
            "reference": job.reference,
            "postcode": job.postcode,
            "baseline_period": (
                job.baseline_start_date,
                job.baseline_end_date,
            ),
            "intervention_period": (
                job.intervention_start_date,
                job.intervention_end_date,
            ),
            "reporting_period": (
                job.reporting_start_date,
                job.reporting_end_date,
            ),
            "reporting_period_365_plus_days": (
                job.reporting_start_date + relativedelta(days=364)
                <= job.reporting_end_date
            ),
            "better_fit": job.better_fit,
        },
        "data": {
            "hourly_model_if": _output_for_if(job, enums.ModelType.HOURLY),
            "daily_model_if": _output_for_if(job, enums.ModelType.DAILY),
            "electricity": {
                "annual": {
                    "lsoa_kwh": lsoa_average.elec_mean if lsoa_average else None,
                    "postcode_kwh": postcode_average.elec_mean
                    if postcode_average
                    else None,
                    "baseline_kwh": job.electricity_baseline_usage,
                },
                "hourly_model": _output_for(
                    job, enums.ModelType.HOURLY, enums.MeterType.ELEC
                ),
                "daily_model": _output_for(
                    job, enums.ModelType.DAILY, enums.MeterType.ELEC
                ),
            }
            if job.analyse_elec
            else None,
            "gas": {
                "annual": {
                    "lsoa_kwh": lsoa_average.gas_mean if lsoa_average else None,
                    "postcode_kwh": postcode_average.gas_mean
                    if postcode_average
                    else None,
                    "baseline_kwh": job.gas_baseline_usage,
                },
                "hourly_model": _output_for(
                    job, enums.ModelType.HOURLY, enums.MeterType.GAS
                ),
                "daily_model": _output_for(
                    job, enums.ModelType.DAILY, enums.MeterType.GAS
                ),
            }
            if job.analyse_gas
            else None,
        }
        if job.processing_finished_at is not None
        else None,
    }
    return render(request, "base/result.html", context)


@login_required
def download_summed_csv(request, pk):
    job = get_object_or_404(AnalysisJob, user=request.user, pk=pk)
    elec_data = job.meteredsavingshourly_set.filter(type="Electricity").values()
    gas_data = job.meteredsavingshourly_set.filter(type="Gas").values()

    generated_data = _generate_list_of_dicts(gas_data, elec_data)

    response = HttpResponse(content_type="text/csv")
    response["Content-Disposition"] = 'attachment; filename="combined_model_data.csv"'
    writer = csv.DictWriter(
        response,
        fieldnames=[
            "Time",
            "Counterfactual Usage",
            "Reporting Observed",
            "Metered Savings",
        ],
    )
    writer.writeheader()
    for data in generated_data:
        writer.writerow(data)

    return response


@login_required
def download_summed_csv_daily(request, pk):
    job = get_object_or_404(AnalysisJob, user=request.user, pk=pk)
    elec_data = job.meteredsavingsdaily_set.filter(type="Electricity").values()
    gas_data = job.meteredsavingsdaily_set.filter(type="Gas").values()

    generated_data = _generate_list_of_dicts(elec_data, gas_data)

    response = HttpResponse(content_type="text/csv")
    response["Content-Disposition"] = 'attachment; filename="combined_model_data.csv"'
    writer = csv.DictWriter(
        response,
        fieldnames=[
            "Time",
            "Counterfactual Usage",
            "Reporting Observed",
            "Metered Savings",
        ],
    )
    writer.writeheader()
    for data in generated_data:
        writer.writerow(data)

    return response


def _generate_list_of_dicts(gas_data, elec_data):
    generated_data = []
    if all((gas_data, elec_data)):
        for a, b in zip(gas_data, elec_data):
            generated_data.append(
                {
                    "Time": a["start"],
                    "Counterfactual Usage": a["counterfactual_usage"]
                    + b["counterfactual_usage"],
                    "Reporting Observed": a["reporting_observed"]
                    + b["reporting_observed"],
                    "Metered Savings": a["metered_savings"] + b["metered_savings"],
                }
            )
    elif elec_data:
        for a in elec_data:
            generated_data.append(
                {
                    "Time": a["start"],
                    "Counterfactual Usage": a["counterfactual_usage"],
                    "Reporting Observed": a["reporting_observed"],
                    "Metered Savings": a["metered_savings"],
                }
            )
    elif gas_data:
        for a in gas_data:
            generated_data.append(
                {
                    "Time": a["start"],
                    "Counterfactual Usage": a["counterfactual_usage"],
                    "Reporting Observed": a["reporting_observed"],
                    "Metered Savings": a["metered_savings"],
                }
            )

    return generated_data


def about(request):
    return render(request, "base/about.html")


def faq(request):
    return render(request, "base/faq.html")
