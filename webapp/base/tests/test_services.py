import datetime
import gzip
import io
from pathlib import Path

import pandas as pd
import pytest
import requests_mock

from webapp.base import enums
from webapp.base import models
from webapp.base import services
from webapp.users import models as user_models


def _from_file(path):
    p = Path(__file__).with_name(path)
    p_gzip = Path(__file__).with_name(path + ".gz")

    if p.exists():
        with p.open("r") as f:
            return f.read()
    elif p_gzip.exists():
        with gzip.open(str(p_gzip), "rb") as f:
            return f.read().decode("utf-8")
    else:
        raise FileNotFoundError(f"Couldn't find file {path} or {path}.gz")


def test_model_runs():
    job = models.AnalysisJob(
        postcode="M15 5EH",
        baseline_start_date=datetime.date(2021, 1, 1),
        baseline_end_date=datetime.date(2022, 1, 1),
        intervention_start_date=datetime.date(2021, 4, 1),
        intervention_end_date=datetime.date(2022, 2, 1),
        reporting_start_date=datetime.date(2022, 2, 2),
        reporting_end_date=datetime.date(2023, 2, 2),
        temperature_data=_from_file("meter_129_weather.csv"),
    )

    gas_csv = _from_file("meter_129_gas.csv")
    fake_file = io.StringIO(gas_csv)
    meter_data = services.format_meter_data(pd.read_csv(fake_file, index_col=0))

    (
        better_fit,
        daily_savings_hourly,
        total_savings_hourly,
        percent_savings_hourly,
        cvrmse_hourly,
        nmbe_hourly,
        rsquared_hourly,
        daily_savings_daily,
        total_savings_daily,
        percent_savings_daily,
        cvrmse_daily,
        nmbe_daily,
        rsquared_daily,
    ) = services.run_caltrack_model(job, "gas", meter_data)

    assert better_fit is not None
    assert daily_savings_hourly is not None
    assert total_savings_hourly is not None
    assert percent_savings_hourly is not None
    assert cvrmse_hourly is not None
    assert nmbe_hourly is not None
    assert rsquared_hourly is not None
    assert daily_savings_daily is not None
    assert total_savings_daily is not None
    assert percent_savings_daily is not None
    assert cvrmse_daily is not None
    assert nmbe_daily is not None
    assert rsquared_daily is not None


def test_fill_data_from_powershaper():
    job = models.AnalysisJob(
        user=user_models.User(powershaper_token="NOT-A-TOKEN"),
        baseline_start_date=datetime.date(2021, 1, 1),
        reporting_end_date=datetime.date(2021, 1, 5),
        psm_consents=["not-a-uuid"],
    )

    with requests_mock.Mocker() as mocker:
        mocker.get(
            "https://app.powershaper.io/meters/api/v1/meter/not-a-uuid/electricity",
            json=[
                {
                    "carbon_kg": 0,
                    "energy_kwh": 3,
                    "error_count": 0,
                    "time": "2021-01-01T00:00:00Z",
                },
                {
                    "carbon_kg": 0,
                    "energy_kwh": 7,
                    "error_count": 0,
                    "time": "2021-01-02T00:00:00Z",
                },
                {
                    "carbon_kg": 0,
                    "energy_kwh": 13,
                    "error_count": 0,
                    "time": "2021-01-03T00:00:00Z",
                },
                {
                    "carbon_kg": 0,
                    "energy_kwh": 21,
                    "error_count": 0,
                    "time": "2021-01-05T23:30:00Z",
                },
            ],
        )
        meter_data = services.fill_data_from_powershaper(job, enums.MeterType.ELEC)

    assert len(meter_data) == 1
    assert meter_data[0].type == enums.MeterType.ELEC
    assert meter_data[0].earliest == datetime.datetime(
        2021, 1, 1, 0, 0, tzinfo=datetime.timezone.utc
    )
    assert meter_data[0].latest == datetime.datetime(
        2021, 1, 5, 23, 30, tzinfo=datetime.timezone.utc
    )
    assert meter_data[0].data == [
        ("2021-01-01T00:00:00+00:00", 3.0),
        ("2021-01-02T00:00:00+00:00", 7.0),
        ("2021-01-03T00:00:00+00:00", 13.0),
        ("2021-01-05T23:30:00+00:00", 21.0),
    ]


def test_date_range_to_datetime_range():
    period = services._date_range_to_datetime_range(
        datetime.date(2021, 1, 1), datetime.date(2021, 1, 2)
    )

    assert period.start == datetime.datetime(
        2021, 1, 1, 0, 0, 0, 0, tzinfo=datetime.timezone.utc
    )
    assert period.end == datetime.datetime(
        2021, 1, 2, 23, 59, 59, 999999, tzinfo=datetime.timezone.utc
    )


@pytest.mark.django_db
def test_model_with_reference_dataset():
    job = models.AnalysisJob(
        id=1,
        postcode="M15 5EH",
        baseline_start_date=datetime.date(2020, 5, 1),
        baseline_end_date=datetime.date(2021, 4, 30),
        intervention_start_date=datetime.date(2021, 5, 1),
        intervention_end_date=datetime.date(2021, 5, 2),
        reporting_start_date=datetime.date(2021, 5, 3),
        reporting_end_date=datetime.date(2022, 5, 3),
        temperature_data=_from_file("meter_129_weather_full.csv"),
        analyse_elec=True,
        analyse_gas=True,
    )

    job.save()

    gas_csv = _from_file("meter_129_gas.csv")
    electricity_csv = _from_file("meter_129_electricity.csv")
    fake_file_gas = io.StringIO(gas_csv)
    fake_file_elec = io.StringIO(electricity_csv)
    meter_data_gas = services.format_meter_data(pd.read_csv(fake_file_gas, index_col=0))
    meter_data_elec = services.format_meter_data(
        pd.read_csv(fake_file_elec, index_col=0)
    )

    services.run_caltrack_model(job, "gas", meter_data_gas)
    services.run_caltrack_model(job, "electricity", meter_data_elec)

    gas_savings_daily = models.MeteredSavingsDaily.objects.filter(
        type="gas", caltrack_id=1
    ).values()

    gas_savings_hourly = models.MeteredSavingsHourly.objects.filter(
        type="gas", caltrack_id=1
    ).values()

    elec_savings_daily = models.MeteredSavingsDaily.objects.filter(
        type="electricity", caltrack_id=1
    ).values()

    elec_savings_hourly = models.MeteredSavingsHourly.objects.filter(
        type="electricity", caltrack_id=1
    ).values()

    def combine_counterfactuals(gas_data, elec_data):
        combined_data = []
        for a, b in zip(gas_data, elec_data):
            combined_data.append(
                {
                    "Time": a["start"],
                    "counterfactual_usage": a["counterfactual_usage"]
                    + b["counterfactual_usage"],
                }
            )
        return pd.DataFrame(combined_data)

    combined_daily = combine_counterfactuals(gas_savings_daily, elec_savings_daily)
    combined_hourly = combine_counterfactuals(gas_savings_hourly, elec_savings_hourly)

    ref_csv_daily = _from_file(
        "meter_129_reference_metered_savings_dataframe_daily.csv"
    )
    fake_file_ref = io.StringIO(ref_csv_daily)
    ref_df_daily = pd.read_csv(fake_file_ref)

    ref_csv_hourly = _from_file(
        "meter_129_reference_metered_savings_dataframe_hourly.csv"
    )
    fake_file_ref = io.StringIO(ref_csv_hourly)
    ref_df_hourly = pd.read_csv(fake_file_ref)

    daily_diff = (
        combined_daily["counterfactual_usage"] - ref_df_daily["counterfactual_usage"]
    ).sum()

    hourly_diff = (
        combined_hourly["counterfactual_usage"] - ref_df_hourly["counterfactual_usage"]
    ).sum()

    assert -10 <= daily_diff < 10
    assert -10 <= hourly_diff < 10
