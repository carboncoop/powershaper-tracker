import pytest


@pytest.mark.django_db
def test_root_page_returns_http_ok(client):
    response = client.get("/")

    assert response.status_code == 200
