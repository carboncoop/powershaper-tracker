from datetime import date
from pathlib import Path

from webapp.base import models
from webapp.base import services


def fetch_weather_data_for_postcode():
    """
    Fetch weather data for a given postcode, put it in a file.

    Not used in automatic runs, but a useful function to have around for writing tests.
    """
    job = models.AnalysisJob(
        postcode="M15 5EH",
        baseline_start_date=date(2021, 1, 1),
        reporting_end_date=date(2023, 2, 5),
    )

    data = services.fetch_temperature_data(job)

    p = Path(__file__).with_name("weather.csv")
    with p.open("w") as f:
        f.write(data.to_csv())
