from django import forms

from webapp.apis import meters


class TokenForm(forms.Form):
    token = forms.CharField(
        label="PowerShaper API token",
        widget=forms.TextInput(attrs={"class": "form-control"}),
    )

    def clean(self):
        data = super().clean()

        meters.get_meterconsent_list(token=data["token"])

        return data
