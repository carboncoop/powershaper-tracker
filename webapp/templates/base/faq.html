{% extends 'main.html' %}

{% block title %}FAQ{% endblock %}

{% block content %}
    <div class="container mt-4">
        <div class="row">
            <div class="col-lg-9 col">

                <h1>FAQ</h1>

                <section class="mb-4">
                    <h2>PowerShaper Tracker</h2>
                    <details>
                        <summary>What is PowerShaper Tracker?</summary>
                        <p>PowerShaper Tracker is a new service developed by Carbon Co-op to enable easy energy baselining for domestic buildings. This means that you can estimate what your energy consumption would have been had you not implemented any energy saving measures, such as turning down the heating, installing insulation, or switching from a gas boiler to electric heat pump.</p>
                    </details>
                    <details>
                        <summary>How does PowerShaper Tracker work?</summary>
                        <p>
                            PowerShaper Tracker uses your historical energy consumption data and local air temperature data to generate a simple building energy model. The basis for the model lies in the <a href="https://www.caltrack.org/">CalTRACK Methods</a>, a model developed in the US and since extended by Carbon Co-op to accommodate additional regions (such as the UK).
                        </p>
                    </details>
                    <details>
                        <summary>How can I get started?</summary>
                        <p>
                            All PowerShaper Tracker needs to operate is your energy consumption data and retrofit installation dates, and we can do the rest.
                        </p>
                    </details>
                    <details>
                        <summary>What does Energy consumption data mean?</summary>
                        <p>
                            If you have already signed up to Carbon Co-op’s <a href="https://powershaper.io/">PowerShaper service</a>, you simply need to <a href="https://app.powershaper.io/meters/signup/">log in</a> and your energy consumption data will be automatically linked to the service. If you are not a PowerShaper subscriber and would not like to become one, you will need to upload your data to the Tracker yourself.
                        </p>
                    </details>
                    <details>
                        <summary>What do the Retrofit installation dates mean?</summary>
                        <p>
                            You will need to specify the implementation dates for any energy efficiency improvements you wish to evaluate: specifically, the installation start date and end dates. The start date should be the first date from which any building works/installations/behaviour changes began to be implemented, and the end date should be the last date works/changes were underway before energy consumption returned to a stable pattern.
                        </p>
                    </details>
                    <details>
                        <summary>Is PowerShaper Tracker the same as the other Carbon Co-op’s PowerShaper services?</summary>
                        <p>
                            PowerShaper Tracker is a measurement and verification service distinct from PowerShaper Monitor and PowerShaper Flex, two other smart meter analytics services owned and operated by Carbon Co-op. For more information about Carbon Co-op’s other PowerShaper services please visit <a href="https://powershaper.io/">PowerShaper Monitor</a> and <a href="https://flex.powershaper.io/">PowerShaper Flex</a>.
                        </p>
                    </details>
                </section>

                <section class="mb-4">
                    <h2>Uncertainty Metrics</h2>

                    <details>
                        <summary>What are uncertainty metrics and how can I interpret them?</summary>
                        <p>Uncertainty metrics are used to evaluate the uncertainty associated with a particular model. The most common metrics used in measurement and verification are CV-RMSE, NMBE, and R-squared.</p>
                    </details>
                    <details>
                        <summary>What does 'CV-RMSE' mean?</summary>
                        <p>CV-RMSE (Coefficient of Variation of the Root Mean Squared Error) is used to evaluate training period model fit. CV-RMSE punishes greater counterfactual deviations from reported consumption, meaning that it well represents pointwise model fit. While there are many thresholds for acceptable CV-RMSE, a common threshold is 0.25 (25%) as seen in ASHRAE Guideline 14 where more than 12 months of post retrofit data are being analysed to compute savings.</p>
                    </details>
                    <details>
                        <summary>What does 'NMBE' mean?</summary>
                        <p>NMBE (Normalised Mean Bias Error) represents the overall cumulative deviation of the counterfactual from reported consumption in the training period. While there are several possible NMBE thresholds, ASHRAE Guideline 14 recommends >±0.005 (±0.5%).</p>
                    </details>
                    <details class="mb-3">
                        <summary>What does 'R-Squared' mean?</summary>
                        <p>R-Squared is a calculation of training period model fit, an alternative to CV-RMSE. R-Squared is equal to (1 - the sum of squares of the training period residuals over the total sum of squares).</p>
                    </details>

                    <p>Further details on these calculations can be found in the <a href="https://eemeter.openee.io/api.html#api-docs">CalTRACK API docs</a>, the <a href="https://docs.caltrack.org/_/downloads/en/stable/pdf/">CalTRACK documentation</a>, and <a href="https://upgreengrade.ir/admin_panel/assets/images/books/ASHRAE%20Guideline%2014-2014.pdf">ASHRAE Guideline 14.</a></p>
                </section>

                <section class="mb-4">
                    <h2>CO2 Emissions Reduction</h2>
                    <details>
                        <summary>How do we calculate CO2 emissions reductions?</summary>
                        <p>The main climate impact of energy use is from the carbon dioxide emissions associated with combustion for the generation of electricity or heat. In the case of electricity this arises from the burning of coal, gas, biomass or waste in a power station, and as such the carbon emitted for a unit of energy (its carbon intensity) varies according to the mix of generation occurring from hour to hour.<br />For the purposes of benchmarking emissions over a longer period, we use the average figure for carbon intensity specified in the government’s 2021 Standard Assessment Procedure for Energy Rating of Dwellings (<a href="https://files.bregroup.com/SAP/SAP%2010.2%20-%2017-12-2021.pdf">P182, SAP version 10.2 - 17-12-2021</a>), currently 136gCO2/kWh for electricity and 210gCO2/kWh for mains gas.</p>
                    </details>
                </section>

                <section class="mb-4">
                    <h2>LSOA and Postcode Annual Usage Data</h2>
                    <details>
                        <summary>What does the LSOA and postcode annual usage data signify?</summary>
                        <p>Local and postcode averages are taken from national datasets made available by from the Department of Business, Energy and Industrial Strategy. These raw averages hide significant variation due to factors such as different space and hot water heating systems. These are provided for general comparison and may not represent a good measure of the performance of your home. Local area data is taken from the <a href"https://www.gov.uk/government/statistics/lower-and-middle-super-output-areas-electricity-consumption">2021 Lower Super Output Area (LSOA) dataset</a>. Postcode data is taken from the <a href="https://www.gov.uk/government/statistics/postcode-level-electricity-statistics-2021-experimental">2021 experimental postcode dataset</a>, and is based on your full postcode.</p>
                    </details>
                </section>

                <section class="mb-4">
                    <h2>CalTRACK</h2>
                    <details>
                        <summary>What is CalTRACK?</summary>
                        <p>CalTRACK is an advanced measurement and verification standard initially developed by Open Energy Efficiency, Inc. (now Recurve, Inc.). CalTRACK has been used to validate savings across several residential energy efficiency programmes in the United States.</p>

                        <p>PowerShaper Tracker uses the CalTRACK Methods, as implemented by the open-source <a href="https://github.com/openeemeter/eemeter">EEMeter</a> and <a href="https://github.com/openeemeter/eeweather">EEWeather</a> packages, to construct counterfactual energy consumption baselines. All calculations are undertaken by EEMeter and EEWeather unless otherwise stated.</p>
                    </details>
                </section>

                <section class="mb-4">
                    <h2>Models</h2>
                    <details>
                        <summary>What do we mean by model fit?</summary>
                        <p>
                            Model fit in PowerShaper Tracker is measured by CV-RMSE value, or the coefficient of variation of the root mean squared error. This metric is generated by evaluating the difference between the counterfactual baseline and observed consumption in the training period: the lower the CV-RMSE value, the better the model fit.
                        </p>
                    </details>
                    <details>
                        <summary>What is the difference between hourly and daily CalTRACK models?</summary>
                        <p>
                            PowerShaper Tracker follows the CalTRACK Methods as implemented by the open-source EEMeter package. EEMeter comprises two types of models able to interpret either hourly or daily/monthly energy consumption data.
                        </p>
                        <p>
                            The hourly model is a Time of week-Temperature (TOWT) model and operates via construction of 12 separate piecewise linear regression models, one for each month of the year. These are then switched on and off on the basis of an assumed occupancy parameter, itself generated by the model's inference of energy consumption routines.
                        </p>
                        <p>
                            The daily model is a simpler model comprising one piecewise linear regression model applied at all time points throughout the year.
                        </p>
                        <p>
                            More information on the CalTRACK Methods and the hourly and daily models can be found at <a href="caltrack.org">caltrack.org</a> and in the <a href="https://docs.caltrack.org/_/downloads/en/stable/pdf/">CalTRACK documentation</a>.
                        </p>
                    </details>
                </section>
            </div>
        </div>
    </div>
{% endblock content %}
