import datetime
import logging
from dataclasses import dataclass

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.cache import cache
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.shortcuts import render
from django.utils import timezone

import webapp.base.enums as base_enums
import webapp.base.models as base_models
import webapp.base.services as base_services
import webapp.base.tasks as base_tasks
from . import forms
from webapp.apis import meters as psm_api

logger = logging.getLogger(__name__)


@login_required
def new_job(request):
    form = forms.NewJob()
    if request.method == "POST":
        form = forms.NewJob(request.POST)
        if form.is_valid():
            job = base_models.AnalysisJob.objects.create(
                user=request.user,
                **form.cleaned_data,
            )
            if job.data_origin == base_enums.DataOrigin.CSV:
                return redirect("new-job:upload-csvs", job.pk)
            else:
                return redirect("new-job:select-meters", job.pk)

    context = {"form": form}
    return render(request, "new-job/start.html", context)


@login_required
def upload_csvs(request, pk):
    # TODO(anna): remove submitted_at from here, instead do the check after the row
    # is found and tell the user the job is no longer amendable
    job = get_object_or_404(
        base_models.AnalysisJob, user=request.user, pk=pk, submitted_at=None
    )

    form = forms.UploadCSVs(
        multiple=job.portfolio_mode == base_enums.PortfolioMode.PORTFOLIO,
        analyse_elec=job.analyse_elec,
        analyse_gas=job.analyse_gas,
    )
    if request.method == "POST":
        form = forms.UploadCSVs(
            request.POST,
            request.FILES,
            multiple=job.portfolio_mode == base_enums.PortfolioMode.PORTFOLIO,
            analyse_elec=job.analyse_elec,
            analyse_gas=job.analyse_gas,
        )
        if form.is_valid():
            if hasattr(form, "gas_data"):
                for data in form.gas_data:
                    data.job = job
                    data.type = base_enums.MeterType.GAS
                    data.save()

            if hasattr(form, "elec_data"):
                for data in form.elec_data:
                    data.job = job
                    data.type = base_enums.MeterType.ELEC
                    data.save()

            return redirect("new-job:finalise", job.pk)

    context = {"form": form}
    return render(request, "new-job/upload-csvs.html", context)


@dataclass
class DateRange:
    earliest: datetime.date
    latest: datetime.date


def _largest_common_range(date_range: list[DateRange]) -> DateRange:
    if len(date_range) == 0:
        return None

    earliest = max(range_.earliest for range_ in date_range)
    latest = min(range_.latest for range_ in date_range)

    if earliest >= latest:
        return None

    return DateRange(earliest=earliest, latest=latest)


@dataclass
class MeterConsent:
    uuid: str
    last_four_digits: str
    postcode: str
    range: DateRange | None


def _get_meterconsents(*, token, analyse_gas, analyse_elec):
    meters = cache.get(f"meterconsents-{token}")
    if meters is None:
        meters = psm_api.get_meterconsent_list(token)
        cache.set(f"meterconsents-{token}", meters, 120)

    # TODO(anna): Handle the case where meters are not available because they don't
    # have the range of data required for analysis.

    meters_by_uuid = {}

    for consent in meters:
        if analyse_gas is False and consent.type == "gas":
            continue
        if analyse_elec is False and consent.type == "electricity":
            continue
        if consent.range is None:
            continue

        if consent.consent_uuid not in meters_by_uuid:
            meters_by_uuid[consent.consent_uuid] = []

        meters_by_uuid[consent.consent_uuid].append(consent)

    result: list[MeterConsent] = []

    for uuid, meters in meters_by_uuid.items():
        if analyse_gas and analyse_elec:
            if len(meters) != 2:
                continue
        else:
            assert len(meters) == 1

        output = MeterConsent(
            uuid=uuid,
            last_four_digits=meters[0].consent_last_four_digits,
            postcode=meters[0].postcode,
            range=_largest_common_range(
                [
                    DateRange(
                        earliest=base_services.first_full_day_from(
                            meter.range.earliest
                        ),
                        latest=base_services.last_complete_day_before(
                            meter.range.latest
                        ),
                    )
                    for meter in meters
                ]
            ),
        )

        if output.range is not None:
            result.append(output)

    return result


@login_required
def select_meters(request, pk):
    # TODO(anna): remove submitted_at from here, instead do the check after the row
    # is found and tell the user the job is no longer amendable
    logger.debug("Ok, selecting meters")

    if not request.user.powershaper_token:
        return redirect("settings")
    job = get_object_or_404(
        base_models.AnalysisJob, user=request.user, pk=pk, submitted_at=None
    )

    logger.debug("About to get meter consents")

    meterconsents = _get_meterconsents(
        token=request.user.powershaper_token,
        analyse_gas=job.analyse_gas,
        analyse_elec=job.analyse_elec,
    )

    logger.debug("Got meter consents")

    if request.method == "POST":
        next_step = False

        if "select-meter" in request.POST:
            selected_uuid = request.POST["select-meter"]
            if not selected_uuid:
                raise ValueError("Meter UUID required")
            if selected_uuid not in [consent.uuid for consent in meterconsents]:
                raise ValueError("Bad UUID")

            job.psm_consents = list(set([selected_uuid, *job.psm_consents]))

            if job.portfolio_mode == base_enums.PortfolioMode.SINGLE:
                next_step = True

        elif "deselect-meter" in request.POST:
            deselected_uuid = request.POST["deselect-meter"]
            if not deselected_uuid:
                raise ValueError("Meter UUID required")
            if deselected_uuid not in [consent.uuid for consent in meterconsents]:
                raise ValueError("Bad UUID")

            job.psm_consents = list(
                uuid for uuid in job.psm_consents if deselected_uuid != uuid
            )

        elif "continue" in request.POST:
            if job.portfolio_mode == base_enums.PortfolioMode.PORTFOLIO:
                next_step = True

        else:
            raise ValueError("Bad form data")

        if next_step:
            selected_meters = list(
                consent for consent in meterconsents if consent.uuid in job.psm_consents
            )
            common_range = _largest_common_range(
                list(meter.range for meter in selected_meters)
            )
            if job.portfolio_mode == base_enums.PortfolioMode.SINGLE:
                job_type = "Single"
            else:
                job_type = "Portfolio"
            if len(selected_meters) == 1:
                job.reference = f"MPAN ending {selected_meters[0].last_four_digits}"
            else:
                all_mpans = [meter.last_four_digits for meter in selected_meters]
                if len(all_mpans) > 3:
                    job.reference = f"{job_type}, MPANs ending {', '.join(all_mpans[:3])}.. ({len(all_mpans)} meters)."
                else:
                    job.reference = f"{job_type}, MPANs ending {', '.join(all_mpans)}. ({len(all_mpans)} meters)."

            job.postcode = selected_meters[0].postcode
            job.psm_range_start = common_range.earliest
            job.psm_range_end = common_range.latest
            job.save()

            return redirect("new-job:finalise", job.pk)
        else:
            job.save()

    selected_meters = list(
        consent for consent in meterconsents if consent.uuid in job.psm_consents
    )
    unselected_meters = list(
        consent for consent in meterconsents if consent.uuid not in job.psm_consents
    )
    common_range = _largest_common_range(list(meter.range for meter in selected_meters))

    if len(set(meter.postcode for meter in selected_meters)) > 1:
        different_postcodes = True
    else:
        different_postcodes = False

    error = None
    if not common_range and len(selected_meters) > 1:
        error = "no overlap"
    elif common_range:
        common_range_delta = common_range.latest - common_range.earliest
        if common_range_delta.days < 366:
            error = "not enough overlap"

    context = {
        "selected_meters": selected_meters,
        "unselected_meters": unselected_meters,
        "common_range": common_range,
        "different_postcodes": different_postcodes,
        "error": error,
    }
    return render(request, "new-job/select-meter.html", context)


@login_required
def finalise(request, pk):
    # TODO(anna): remove submitted_at from here, instead do the check after the row
    # is found and tell the user the job is no longer amendable
    job = get_object_or_404(
        base_models.AnalysisJob, user=request.user, pk=pk, submitted_at=None
    )

    form = forms.Finalise(
        date_range=base_services.get_date_range(job),
        initial={
            "postcode": job.postcode,
            "reference": job.reference,
        },
    )
    if request.method == "POST":
        form = forms.Finalise(
            request.POST, date_range=base_services.get_date_range(job)
        )
        if form.is_valid():
            job.submitted_at = timezone.now()
            job.reference = form.cleaned_data["reference"]
            job.floor_area = form.cleaned_data["floor_area"]
            job.postcode = form.cleaned_data["postcode"]
            job.intervention_start_date = form.cleaned_data["intervention_start_date"]
            job.intervention_end_date = form.cleaned_data["intervention_start_date"]
            base_services.fetch_lsoa(job)
            base_services.set_dates_based_on_intervention_period(
                job,
                form.cleaned_data["intervention_start_date"],
                form.cleaned_data["intervention_end_date"],
            )
            job.save()

            base_tasks.fetch_powershaper_data.delay(
                job.pk, job_id=f"fetch_powershaper_{job.pk}"
            )

            messages.add_message(
                request,
                messages.INFO,
                "Your analysis job has been submitted, but it might take up to 20"
                " minutes before results appear.",
            )
            return redirect("result", pk=job.pk)

    context = {"form": form}
    return render(request, "new-job/finalise.html", context)
