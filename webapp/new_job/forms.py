import csv
import datetime
import io

import pandas as pd
from dateutil.parser import isoparse
from dateutil.parser import ParserError
from dateutil.relativedelta import relativedelta
from django import forms

import webapp.base.enums as base_enums
import webapp.base.models as base_models
from webapp.base import normalise_postcode
from webapp.base import validate_household_postcode


class NewJob(forms.Form):
    portfolio_mode = forms.ChoiceField(
        label="Do you want to analyse an individual set of a results or a portfolio?",
        choices=base_enums.PortfolioMode.choices,
        widget=forms.RadioSelect(),
    )

    data_origin = forms.ChoiceField(
        label="Where do you want to source the data from?",
        choices=base_enums.DataOrigin.choices,
        widget=forms.RadioSelect(),
    )

    analyse_gas = forms.BooleanField(
        label="Analyse gas data?", initial=True, required=False
    )
    analyse_elec = forms.BooleanField(
        label="Analyse electricity data?", initial=True, required=False
    )


class UploadCSVs(forms.Form):
    gas_files = forms.FileField(required=True)
    elec_files = forms.FileField(required=True)

    # TODO(anna): depending on whether analyse_gas & analyse_elec are selected, we
    # sould present different fields

    def __init__(self, *args, **kwargs):
        self.analyse_gas = kwargs.pop("analyse_gas")
        self.analyse_elec = kwargs.pop("analyse_elec")
        self.multiple = kwargs.pop("multiple")
        self.err_files = []
        self.value_err_files = []

        super().__init__(*args, **kwargs)
        self.fields["elec_files"].label = "Electricity files"

        if self.multiple:
            self.fields["gas_files"].widget = forms.ClearableFileInput(
                attrs={"multiple": True}
            )
            self.fields["elec_files"].widget = forms.ClearableFileInput(
                attrs={"multiple": True}
            )

        if not self.analyse_gas:
            del self.fields["gas_files"]

        if not self.analyse_elec:
            del self.fields["elec_files"]

    def clean_gas_files(self):
        data = super().clean()

        if self.multiple:
            files = self.files.getlist("gas_files")
            if len(files) <= 1:
                raise forms.ValidationError(
                    "there should at least be two or more gas files uploaded"
                )
        else:
            files = [data["gas_files"]]
        file_type = "gas"
        parsed = [self._parse_csv(file, file_type) for file in files]

        if len(self.err_files) > 0:
            if self.analyse_gas and not self.analyse_elec:
                for i in self.err_files:
                    self.add_error(
                        "gas_files",
                        forms.ValidationError(f"{i}"),
                    )
                for i in self.value_err_files:
                    self.add_error(
                        "gas_files",
                        forms.ValidationError(f"{i}"),
                    )

        if not self.errors:
            self.gas_data = parsed

        return data

    def clean_elec_files(self):
        data = super().clean()

        if self.multiple:
            files = self.files.getlist("elec_files")
            if len(files) <= 1:
                raise forms.ValidationError(
                    "there should at least be two or more electricity files uploaded"
                )
        else:
            files = [data["elec_files"]]
        file_type = "elec"
        parsed = [self._parse_csv(file, file_type) for file in files]

        if len(self.err_files) > 0 or len(self.value_err_files) > 0:
            if self.analyse_elec and not self.analyse_gas:
                for i in self.err_files:
                    self.add_error(
                        "elec_files",
                        forms.ValidationError(f"{i}"),
                    )
                for i in self.value_err_files:
                    self.add_error(
                        "elec_files",
                        forms.ValidationError(f"{i}"),
                    )
            if self.analyse_elec and self.analyse_gas:
                for i in self.err_files:
                    self.add_error(
                        "gas_files",
                        forms.ValidationError(f"{i}"),
                    )
                for i in self.value_err_files:
                    self.add_error(
                        "gas_files",
                        forms.ValidationError(f"{i}"),
                    )

        if not self.errors:
            self.elec_data = parsed

        return data

    def clean(self):
        data = super().clean()

        if hasattr(self, "elec_data") and hasattr(self, "gas_data"):
            if len(self.elec_data) != len(self.gas_data):
                raise forms.ValidationError(
                    "there should be the same amount of gas and electricity files uploaded"
                )
        return data

    def _parse_csv(self, file, file_type) -> base_models.MeterData | None:
        """Parse a CSV file into an incomplete MeterData record."""
        text_wrapper = io.TextIOWrapper(file.file, encoding="utf-8")
        reader = csv.reader(text_wrapper)
        headers = None

        result = []
        for row in reader:
            if headers is None:
                headers = row
                if headers[0].lower() not in ["datetime", "timestamp", "index"]:
                    raise forms.ValidationError(
                        f"Header of first column in {file.name} is '{headers[0]}' but"
                        " should be 'datetime', 'timestamp', 'index' (case insensitive)"
                    )
                if headers[1].lower() != "value":
                    raise forms.ValidationError(
                        f"Header of first column in {file.name} is not 'value'"
                        " (case insensitive)"
                    )
            else:
                # The first one should be an ISO timestamp

                try:
                    timestamp = isoparse(row[0])
                # This should just be ParserError but if there's an unexpected failure
                # within the parser then we can get a ValueError
                # https://github.com/dateutil/dateutil/issues/1265
                except (ParserError, ValueError):
                    self.add_error(
                        "gas_files",
                        forms.ValidationError(
                            f"Line {reader.line_num} of {file.name} contains '{row[0]}'"
                            " as the timestamp, which is not a valid timestamp"
                        ),
                    )

                # The second one should be a number
                try:
                    value = float(row[1])
                except ValueError:
                    self.add_error(
                        "gas_files",
                        forms.ValidationError(
                            f"Line {reader.line_num} of {file.name} contains '{row[1]}'"
                            " as the value, which is not a number"
                        ),
                    )

                if not self.errors:
                    result.append((timestamp, value))

        result.sort()
        df = pd.DataFrame.from_records(result, columns=["date", "value"])
        deltas = df["date"].diff()[1:]
        gaps = deltas[deltas > datetime.timedelta(minutes=30)]
        total_minutes_diff = df["date"].iloc[-1] - df["date"].iloc[0]
        total_minutes = ((total_minutes_diff.days * 12) * 60) + (
            total_minutes_diff.seconds / 60
        )

        column_name = "value"

        # Count the occurrences of 0, NaN, and non-number values in the column
        num_zeros = (df[column_name] == 0).sum()
        num_nan = df[column_name].isna().sum()
        num_non_number = pd.to_numeric(df[column_name], errors="coerce").isna().sum()

        # Calculate the total number of values in the column
        total_values = len(df[column_name])

        # Calculate the total percentage
        if file_type == "elec":
            percentage_total = (
                (num_zeros + num_nan + num_non_number) / total_values
            ) * 100
        else:
            percentage_total = ((num_nan + num_non_number) / total_values) * 100
        if percentage_total > 10:
            rounded_percentage_total = round(percentage_total, 2)
            self.value_err_files.append(
                f"{rounded_percentage_total}% of data has gaps in value, which is above the allowed limit of 10%"
                f" in {file.name}."
            )
        if len(gaps) > 0:
            if total_minutes > 0:
                gaps_length = (((gaps.sum().days) * 12) * 60) + (
                    (gaps.sum().seconds) / 60
                )
                gap_percentage = (gaps_length / total_minutes) * 100
                rounded_gap_percentage = round(gap_percentage, 2)
                i = gaps.index[0]
                g = gaps.iloc[0]
                gap_start = df.loc[i - 1, "date"]
                gap_start_date = datetime.datetime.strftime(
                    gap_start, "%d/%m/%Y, %H:%M:%S"
                )
                gap_start_duration = str(g.to_pytimedelta())
                if gap_percentage > 10:
                    self.err_files.append(
                        f"{rounded_gap_percentage}% of data has gaps in date, which is above the allowed limit of 10%"
                        f" in {file.name}, first starting on {gap_start_date} with a duration"
                        f" of {gap_start_duration}."
                    )
        if not self.errors:
            earliest_timestamp = result[0][0]
            latest_timestamp = result[-1][0]

            return base_models.MeterData(
                earliest=earliest_timestamp,
                latest=latest_timestamp,
                data=[(timestamp.isoformat(), value) for timestamp, value in result],
            )
        else:
            return None


class Finalise(forms.Form):
    postcode = forms.CharField(widget=forms.TextInput(attrs={"class": "form-control"}))
    reference = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={"class": "form-control"}),
    )
    floor_area = forms.FloatField(
        label="Floor area in m²",
        help_text=(
            "You may be able to find a floor area figure by "
            "<a target='_blank' href='https://www.gov.uk/find-energy-certificate'>"
            "looking at the EPC database</a>."
        ),
        required=False,
        widget=forms.TextInput(attrs={"class": "form-control"}),
    )
    intervention_start_date = forms.DateField(
        label="Intervention start date:",
        widget=forms.DateInput(
            format=("%Y-%m-%d"),
            attrs={
                "class": "form-control",
                "type": "date",
            },
        ),
    )
    intervention_end_date = forms.DateField(
        label="Intervention end date:",
        widget=forms.DateInput(
            format=("%Y-%m-%d"),
            attrs={
                "class": "form-control",
                "type": "date",
            },
        ),
    )

    def __init__(self, *args, **kwargs):
        date_range = kwargs.pop("date_range")

        super().__init__(*args, **kwargs)

        self.selectable_start_range = (
            date_range[0] + relativedelta(days=364),
            date_range[1] - relativedelta(days=7),
        )
        self.fields["intervention_start_date"].help_text = (
            f"Select a date between "
            f" {self.selectable_start_range[0].strftime('%d/%m/%Y')} and"
            f" {self.selectable_start_range[1].strftime('%d/%m/%Y')}"
        )

        self.latest_meter_date = date_range[1]
        self.latest_weather_date = datetime.date.today() - datetime.timedelta(days=6)
        self.latest_date = min(self.latest_meter_date, self.latest_weather_date)
        self.fields["intervention_end_date"].help_text = (
            "Select a date between the start date and"
            f" {self.latest_date.strftime('%d/%m/%Y')}"
        )

    def clean_postcode(self):
        postcode = self.cleaned_data["postcode"]
        postcode = normalise_postcode(postcode)
        if not validate_household_postcode(postcode):
            self.add_error(
                "postcode",
                forms.ValidationError("This is not a valid UK domestic postcode"),
            )

        return postcode

    def clean_intervention_start_date(self):
        start_date = self.cleaned_data["intervention_start_date"]

        if start_date < self.selectable_start_range[0]:
            # Because there is no nice way to say "please choose this date or a later
            # one", so we ask for after the day before the start of the range.
            date_after = self.selectable_start_range[0] - relativedelta(days=1)
            self.add_error(
                "intervention_start_date",
                forms.ValidationError(
                    "This date is too early because meter data does not go back this"
                    " far. Select a date after"
                    f" {date_after.strftime('%d/%m/%Y')}."
                ),
            )

        if start_date > self.selectable_start_range[1]:
            self.add_error(
                "intervention_start_date",
                forms.ValidationError(
                    "This date is too late because 365 days of data are required before"
                    " the intervention period. Select a date before "
                    f" {self.selectable_start_range[1].strftime('%d/%m/%Y')}."
                ),
            )

        return start_date

    def clean_intervention_end_date(self):
        end_date = self.cleaned_data["intervention_end_date"]

        if end_date > self.latest_meter_date:
            self.add_error(
                "intervention_end_date",
                forms.ValidationError(
                    f"This date is too late because the meter data ends at"
                    f" {self.latest_date.strftime('%d/%m/%Y')}. Select an"
                    f" earlier date."
                ),
            )
        elif end_date > self.latest_weather_date:
            self.add_error(
                "intervention_end_date",
                forms.ValidationError(
                    f"This date is too late because temperature data is only available"
                    f" a week in arrears. Select a date before "
                    f" ({self.latest_date.strftime('%d/%m/%Y')})."
                ),
            )

        return end_date

    def clean(self):
        data = super().clean()

        if data["intervention_end_date"] < data["intervention_start_date"]:
            self.add_error(
                "intervention_end_date",
                forms.ValidationError("The end date must be after the start date."),
            )

        return data
