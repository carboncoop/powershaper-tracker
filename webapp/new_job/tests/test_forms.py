import datetime

from django.core.files.uploadedfile import SimpleUploadedFile

from .. import forms


def _fake_file(contents):
    return SimpleUploadedFile(name="data.csv", content=contents, content_type="str")


def test_well_formed_file_succeeds():
    correct_file = (
        b"Datetime,value,error\n"
        b"2022-09-22 03:00:00Z,0.039\n"
        b"2022-09-22 02:30:00Z,0.017\n"
        b"2022-09-22 01:30:00Z,0.023\n"
        b"2022-09-22 01:00:00Z,0.057\n"
        b"2022-09-21 22:30:00Z,0.032\n"
        b"2022-09-22 00:30:00Z,0.017\n"
        b"2022-09-22 02:00:00Z,0.017\n"
        b"2022-09-22 00:00:00Z,0.017\n"
        b"2022-09-21 23:30:00Z,0.018\n"
        b"2022-09-21 23:00:00Z,0.056\n"
    )

    form = forms.UploadCSVs(
        {},
        {"gas_files": _fake_file(correct_file)},
        analyse_elec=False,
        analyse_gas=True,
        multiple=False,
    )

    assert form.is_valid() is True
    assert len(form.gas_data) == 1
    assert form.gas_data[0].latest == datetime.datetime(
        2022, 9, 22, 3, 0, tzinfo=datetime.timezone.utc
    )
    assert form.gas_data[0].earliest == datetime.datetime(
        2022, 9, 21, 22, 30, tzinfo=datetime.timezone.utc
    )
    assert form.gas_data[0].data == [
        ("2022-09-21T22:30:00+00:00", 0.032),
        ("2022-09-21T23:00:00+00:00", 0.056),
        ("2022-09-21T23:30:00+00:00", 0.018),
        ("2022-09-22T00:00:00+00:00", 0.017),
        ("2022-09-22T00:30:00+00:00", 0.017),
        ("2022-09-22T01:00:00+00:00", 0.057),
        ("2022-09-22T01:30:00+00:00", 0.023),
        ("2022-09-22T02:00:00+00:00", 0.017),
        ("2022-09-22T02:30:00+00:00", 0.017),
        ("2022-09-22T03:00:00+00:00", 0.039),
    ]


def test_malformed_csv_file_fails():
    malformed_file = (
        "timestamp,value\n"
        "2022-12-12,12322,4345\n"
        "2022-12-12,waaaa\n"
        "what fresh hell,is this\n"
        "2022-12-1-12,12322,4345\n"
    ).encode("utf-8")

    form = forms.UploadCSVs(
        {},
        {"gas_files": _fake_file(malformed_file)},
        analyse_elec=False,
        analyse_gas=True,
        multiple=False,
    )

    assert form.is_valid() is False
    assert form.errors == {
        "gas_files": [
            "Line 3 of data.csv contains 'waaaa' as the value, which is not a number",
            "Line 4 of data.csv contains 'what fresh hell' as the timestamp, which is"
            " not a valid timestamp",
            "Line 4 of data.csv contains 'is this' as the value, which is not a number",
            "Line 5 of data.csv contains '2022-12-1-12' as the timestamp, which is not"
            " a valid timestamp",
        ]
    }
