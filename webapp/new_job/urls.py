from django.urls import path

from . import views

app_name = "new-job"

urlpatterns = [
    path("", views.new_job, name="start"),
    path("<int:pk>/upload-csvs/", views.upload_csvs, name="upload-csvs"),
    path("<int:pk>/select-meters/", views.select_meters, name="select-meters"),
    path("<int:pk>/finalise/", views.finalise, name="finalise"),
]
