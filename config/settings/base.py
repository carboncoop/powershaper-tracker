"""
Base settings to build other settings files upon.
"""
import logging
from pathlib import Path

import environ
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration
from sentry_sdk.integrations.logging import LoggingIntegration
from ssm_parameter_store import EC2ParameterStore


BASE_DIR = Path(__file__).resolve().parent.parent.parent
APP_DIR = BASE_DIR / "webapp"

env = environ.Env()


# Get parameters and populate os.environ, if desired
AWS_PARAM_STORE = env("AWS_PARAM_STORE", default="")
if AWS_PARAM_STORE != "":
    store = EC2ParameterStore()
    params = store.get_parameters_by_path(f"/{AWS_PARAM_STORE}/", strip_path=True)
    EC2ParameterStore.set_env(params)

READ_DOT_ENV_FILE = env.bool("DJANGO_READ_DOT_ENV_FILE", default=False)
if READ_DOT_ENV_FILE:
    # OS environment variables take precedence over variables from .env
    env.read_env(str(BASE_DIR / ".env"))

# GENERAL
# ------------------------------------------------------------------------------
ENV = env("ENV", default="staging")  # can also be 'production'
DEBUG = False
TIME_ZONE = "UTC"
LANGUAGE_CODE = "en-gb"
USE_I18N = True
USE_TZ = True

# The externally-accessible URL of the site
# This is used to tell external apps where we are. e.g. Auth0 post-login redirects
SITE_URL = env.str("SITE_URL", "")

ALLOWED_HOSTS = []


# Sentry
# ------------------------------------------------------------------------------
# We set up Sentry early on so that other config errors get logged to Sentry.
# If no DSN is provided, then we skip setup, that's fine.
SENTRY_DSN = env("SENTRY_DSN", default="")
if SENTRY_DSN:
    SENTRY_LOG_LEVEL = env.int("DJANGO_SENTRY_LOG_LEVEL", logging.INFO)

    sentry_logging = LoggingIntegration(
        level=SENTRY_LOG_LEVEL,  # Capture info and above as breadcrumbs
        event_level=logging.ERROR,  # Send events from Error messages
    )

    # This commit ID is substituted in during the Docker build process
    COMMIT_ID = "@@__COMMIT_ID__@@"
    if COMMIT_ID != ("@@" + "__COMMIT_ID__" + "@@"):
        SENTRY_RELEASE = f"openeneffs@{COMMIT_ID}"
    else:
        SENTRY_RELEASE = None

    sentry_sdk.init(
        dsn=SENTRY_DSN,
        integrations=[sentry_logging, DjangoIntegration()],
        environment=ENV,
        release=SENTRY_RELEASE,
        traces_sample_rate=0.1,
    )

    # DisallowedHost errors are basically spam
    from sentry_sdk.integrations.logging import ignore_logger

    ignore_logger("django.security.DisallowedHost")


# DATABASES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {"default": env.db("DATABASE_URL", default="")}
DATABASES["default"]["ATOMIC_REQUESTS"] = True

# https://docs.djangoproject.com/en/4.1/ref/settings/#default-auto-field
DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.db.DatabaseCache",
        "LOCATION": "cachetable",
    }
}


# URLS
# ------------------------------------------------------------------------------
ROOT_URLCONF = "config.urls"
WSGI_APPLICATION = "config.wsgi.application"


# APPS
# ------------------------------------------------------------------------------
LOCAL_APPS = [
    "webapp.users",
    "webapp.base",
    "webapp.new_job",
    "webapp.datasets",
]
INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "social_django",
    "django_rq",
    "bootstrap_datepicker_plus",
    *LOCAL_APPS,
    "mathfilters",
]


# AUTHENTICATION
# ------------------------------------------------------------------------------
# USE_AUTH_SERVICE toggles whether we use the external IdP (Auth0)
USE_AUTH_SERVICE = env.bool("USE_AUTH_SERVICE", default=False)

if USE_AUTH_SERVICE is True:
    # https://docs.djangoproject.com/en/dev/ref/settings/#authentication-backends
    AUTHENTICATION_BACKENDS = [
        "webapp.users.backends.Auth0",
        "django.contrib.auth.backends.ModelBackend",
    ]
    # https://docs.djangoproject.com/en/dev/ref/settings/#login-url
    LOGIN_URL = "/login/auth0"

    # https://auth0.com/blog/django-tutorial-building-and-securing-web-applications/#Integrating.Django.and.Auth0
    SOCIAL_AUTH_TRAILING_SLASH = False
    SOCIAL_AUTH_LOGIN_ERROR_URL = "/login-error/"
    SOCIAL_AUTH_AUTH0_DOMAIN = env.str("AUTH0_DOMAIN")
    SOCIAL_AUTH_AUTH0_KEY = env.str("AUTH0_CLIENT_ID")
    SOCIAL_AUTH_AUTH0_SECRET = env.str("AUTH0_CLIENT_SECRET")
    SOCIAL_AUTH_AUTH0_SCOPE = ["openid", "profile"]
    AUTH0_ENDPOINT = env.str("AUTH0_API_DOMAIN")
    AUTH0_DB_NAME = env.str("AUTH0_DB_NAME")
else:
    # https://docs.djangoproject.com/en/dev/ref/settings/#authentication-backends
    AUTHENTICATION_BACKENDS = ["django.contrib.auth.backends.ModelBackend"]
    # https://docs.djangoproject.com/en/dev/ref/settings/#login-url
    LOGIN_URL = "/login/"

# https://docs.djangoproject.com/en/dev/ref/settings/#auth-user-model
AUTH_USER_MODEL = "users.User"
# https://docs.djangoproject.com/en/dev/ref/settings/#login-redirect-url
LOGIN_REDIRECT_URL = "/"
# https://docs.djangoproject.com/en/dev/ref/settings/#logout-redirect-url
LOGOUT_REDIRECT_URL = "/"


# PASSWORDS
# ------------------------------------------------------------------------------

# Password validation
# https://docs.djangoproject.com/en/4.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]


# MIDDLEWARE
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#middleware
MIDDLEWARE = [
    "config.middleware.HealthCheckMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]


# STATIC
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = str(BASE_DIR / "staticfiles")
# https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = "/static/"
# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = [APP_DIR / "static"]


# TEMPLATES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#templates

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [APP_DIR / "templates"],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]


# ADMIN
# ------------------------------------------------------------------------------
# Django Admin URL.
ADMIN_URL = "admin/"


# LOGGING
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#logging
# See https://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "%(levelname)s %(asctime)s %(module)s "
            "%(process)d %(thread)d %(message)s"
        }
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        }
    },
    "root": {"level": "DEBUG", "handlers": ["console"]},
}

if ENV == "offline":
    RQ_QUEUES = {}
else:
    RQ_QUEUES = {
        "default": {
            "HOST": env.str("REDIS_HOST"),
            "PORT": env.int("REDIS_PORT", default=6379),
            "PASSWORD": env.str("REDIS_PASSWORD", default=""),
            "SSL": env.bool("REDIS_SSL", default=False),
            "DB": env.int("REDIS_DB", default=0),
            "DEFAULT_TIMEOUT": 3600,
        }
    }
    RQ_SHOW_ADMIN_LINK = True
    RQ_EXCEPTION_HANDLERS = [
        "webapp.base.tasks.custom_handler"
    ]  # If you need custom exception handlers
