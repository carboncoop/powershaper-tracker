import os

os.environ["DJANGO_READ_DOT_ENV_FILE"] = "true"
from .base import *  # noqa

SECRET_KEY = "insecure"
