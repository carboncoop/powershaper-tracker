import eemeter

gas, gas_temperature, gas_metadata = eemeter.load_sample("il-gas-hdd-only-hourly")
# note sample temperature data corrresponds to USAF ID 724390, Springfield Capital
# Airport via TMY3 database. ZIP code IL 62707.

gas.to_csv("gas_sample.csv")
gas_temperature.to_csv("temperature_sample.csv")
