# PowerShaper Tracker

A tool developed by [Carbon Co-op](https://carbon.coop) which implements the [CalTRACK methodology](https://www.caltrack.org/) to calculate avoided energy use.

This project provides a web interface to [EEMeter](https://www.lfenergy.org/projects/openeemeter/).

Ultimately we will develop an open-source software product for use by retrofit evaluators, network analysts and financial institutions to validate savings from energy efficiency interventions.

## Data sources
![Data flow diagram](docs/openeneffs-diagram.png "Data Flow Diagram")

The application will pull individual UK smart meter data from the [UK DCC](https://www.smartdcc.co.uk/) via the [Carbon Co-op's PowerShaper](https://gitlab.com/carboncoop/powershaper) application.

## Documentation
For some limited developer documentation, please look in the `docs/` directory.

## Deployment

The deployment to staging happens automatically via the webapp-deployment repo. To deploy to production, create a merge request from staging branch to production.

## License

Apache License 2.0
&copy; Carbon Co-op
